-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Creato il: Mar 05, 2024 alle 07:37
-- Versione del server: 8.0.29
-- Versione PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webchat`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `amministratori_gruppi`
--

CREATE TABLE `amministratori_gruppi` (
  `dataInizio` datetime NOT NULL,
  `dataFine` datetime NOT NULL,
  `utenteUsername` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `gruppoID` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `argomenti`
--

CREATE TABLE `argomenti` (
  `nomeArgomento` varchar(50) COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `chattare_gruppi`
--

CREATE TABLE `chattare_gruppi` (
  `ID` int NOT NULL,
  `messaggio` text COLLATE utf8mb4_general_ci NOT NULL,
  `orarioInvio` datetime NOT NULL,
  `stato` char(1) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'S = spedito\r\nC = consegnato',
  `utenteUsername` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `gruppoID` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `chattare_utenti`
--

CREATE TABLE `chattare_utenti` (
  `ID` int NOT NULL,
  `messaggio` text COLLATE utf8mb4_general_ci NOT NULL,
  `orarioInvio` datetime NOT NULL,
  `orarioLettura` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `stato` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'S = spedito\r\nL = letto',
  `mittenteUsername` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `destinatarioUsername` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dump dei dati per la tabella `chattare_utenti`
--

INSERT INTO `chattare_utenti` (`ID`, `messaggio`, `orarioInvio`, `orarioLettura`, `stato`, `mittenteUsername`, `destinatarioUsername`) VALUES
(3, 'ciao luigi', '2024-03-02 09:38:12', '2000-01-01 00:00:00', 'S', 'Maury4828', 'luigimarroni'),
(4, 'ciao mario', '2024-03-02 09:38:56', '2000-01-01 00:00:00', 'S', 'Maury4828', 'MarioRoss'),
(5, 'ciao mauri', '2024-03-02 09:41:37', '2000-01-01 00:00:00', 'S', 'luigimarroni', 'Maury4828'),
(6, 'oaic', '2024-03-02 09:42:20', '2000-01-01 00:00:00', 'S', 'MarioRoss', 'Maury4828'),
(7, 'ciao ciao ciao', '2024-03-02 10:15:09', '2000-01-01 00:00:00', 'S', 'luigimarroni', 'MarioRoss'),
(8, 'ciaoooo', '2024-03-02 10:58:37', '2000-01-01 00:00:00', 'S', 'Maury4828', 'luigimarroni'),
(9, 'ciao ancora luigi', '2024-03-04 10:10:18', '2000-01-01 00:00:00', 'S', 'Maury4828', 'luigimarroni'),
(10, 'ciaooone', '2024-03-04 11:30:08', '2000-01-01 00:00:00', 'S', 'Maury4828', 'MarioRoss');

-- --------------------------------------------------------

--
-- Struttura della tabella `gruppi`
--

CREATE TABLE `gruppi` (
  `ID` int NOT NULL,
  `nome` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `descrizione` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `dataCreazione` datetime NOT NULL,
  `argomento` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `immagineGruppo` int NOT NULL,
  `creatoreGruppo` varchar(25) COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `partecipare_gruppi`
--

CREATE TABLE `partecipare_gruppi` (
  `dataEntrata` datetime NOT NULL,
  `dataUscita` datetime NOT NULL,
  `utenteUsername` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `gruppoID` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `ricevere_msg_gruppi`
--

CREATE TABLE `ricevere_msg_gruppi` (
  `orarioLettura` datetime NOT NULL,
  `destinatarioUsername` char(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `chattare_gruppi_ID` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `richieste_amicizia`
--

CREATE TABLE `richieste_amicizia` (
  `ID` int NOT NULL,
  `orarioInvio` datetime NOT NULL,
  `orarioAccettata` datetime DEFAULT '2000-01-01 00:00:00',
  `nickMittente` varchar(25) COLLATE utf8mb4_general_ci NOT NULL,
  `nickDestinatario` varchar(25) COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dump dei dati per la tabella `richieste_amicizia`
--

INSERT INTO `richieste_amicizia` (`ID`, `orarioInvio`, `orarioAccettata`, `nickMittente`, `nickDestinatario`) VALUES
(70, '2024-02-24 15:25:54', '2024-02-24 15:27:21', 'MarioRoss', 'Maury4828'),
(73, '2024-02-27 08:51:02', '2024-02-27 08:51:17', 'Maury4828', 'luigimarroni'),
(75, '2024-03-02 10:07:47', '2024-03-02 10:07:58', 'MarioRoss', 'luigimarroni');

-- --------------------------------------------------------

--
-- Struttura della tabella `utenti`
--

CREATE TABLE `utenti` (
  `nome` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `cognome` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `dataNascita` date NOT NULL,
  `userName` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dump dei dati per la tabella `utenti`
--

INSERT INTO `utenti` (`nome`, `cognome`, `dataNascita`, `userName`, `email`, `password`) VALUES
('Luigi', 'Marroni', '2000-01-01', 'luigimarroni', 'luigimarroni@gmail.com', 'aca20b1506ce9a3e713097a375b821dc'),
('Mario', 'Rossi', '2000-01-01', 'MarioRoss', 'mariorossi@gmail.com', '6f3240543cede5da63dc8d44bba868ac'),
('Maurizio', 'Dello Preite', '2004-11-08', 'Maury4828', 'mauriziodellopreite11@gmail.com', 'f8402a6961984459e8113e2f14c90110');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `amministratori_gruppi`
--
ALTER TABLE `amministratori_gruppi`
  ADD KEY `utenteCF` (`utenteUsername`),
  ADD KEY `gruppoID` (`gruppoID`);

--
-- Indici per le tabelle `argomenti`
--
ALTER TABLE `argomenti`
  ADD KEY `nomeArgomento` (`nomeArgomento`);

--
-- Indici per le tabelle `chattare_gruppi`
--
ALTER TABLE `chattare_gruppi`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `utenteCF` (`utenteUsername`),
  ADD KEY `gruppoID` (`gruppoID`);

--
-- Indici per le tabelle `chattare_utenti`
--
ALTER TABLE `chattare_utenti`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `mittenteCF` (`mittenteUsername`),
  ADD KEY `destinatarioCF` (`destinatarioUsername`);

--
-- Indici per le tabelle `gruppi`
--
ALTER TABLE `gruppi`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `argomento` (`argomento`),
  ADD KEY `creatoreGruppo` (`creatoreGruppo`);

--
-- Indici per le tabelle `partecipare_gruppi`
--
ALTER TABLE `partecipare_gruppi`
  ADD KEY `utenteCF` (`utenteUsername`),
  ADD KEY `gruppoID` (`gruppoID`);

--
-- Indici per le tabelle `ricevere_msg_gruppi`
--
ALTER TABLE `ricevere_msg_gruppi`
  ADD KEY `destinatarioCF` (`destinatarioUsername`),
  ADD KEY `chattare_gruppi_ID` (`chattare_gruppi_ID`);

--
-- Indici per le tabelle `richieste_amicizia`
--
ALTER TABLE `richieste_amicizia`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `nickMittente` (`nickMittente`),
  ADD KEY `nickDestinatario` (`nickDestinatario`);

--
-- Indici per le tabelle `utenti`
--
ALTER TABLE `utenti`
  ADD PRIMARY KEY (`userName`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `chattare_gruppi`
--
ALTER TABLE `chattare_gruppi`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `chattare_utenti`
--
ALTER TABLE `chattare_utenti`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT per la tabella `gruppi`
--
ALTER TABLE `gruppi`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `richieste_amicizia`
--
ALTER TABLE `richieste_amicizia`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `amministratori_gruppi`
--
ALTER TABLE `amministratori_gruppi`
  ADD CONSTRAINT `amministrare_gruppi_gruppoID` FOREIGN KEY (`gruppoID`) REFERENCES `gruppi` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `amministrare_gruppi_utenteUsername` FOREIGN KEY (`utenteUsername`) REFERENCES `utenti` (`userName`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limiti per la tabella `chattare_gruppi`
--
ALTER TABLE `chattare_gruppi`
  ADD CONSTRAINT `chattare_gruppi_gruppoID` FOREIGN KEY (`gruppoID`) REFERENCES `gruppi` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `chattare_gruppi_utenteUsername` FOREIGN KEY (`utenteUsername`) REFERENCES `utenti` (`userName`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limiti per la tabella `chattare_utenti`
--
ALTER TABLE `chattare_utenti`
  ADD CONSTRAINT `chattare_utenti_destinatarioUsername` FOREIGN KEY (`destinatarioUsername`) REFERENCES `utenti` (`userName`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `chattare_utenti_mittenteUsername` FOREIGN KEY (`mittenteUsername`) REFERENCES `utenti` (`userName`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limiti per la tabella `gruppi`
--
ALTER TABLE `gruppi`
  ADD CONSTRAINT `gruppi_creatoreGruppo` FOREIGN KEY (`creatoreGruppo`) REFERENCES `utenti` (`userName`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `gruppi_nomeArgomento` FOREIGN KEY (`argomento`) REFERENCES `argomenti` (`nomeArgomento`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limiti per la tabella `partecipare_gruppi`
--
ALTER TABLE `partecipare_gruppi`
  ADD CONSTRAINT `partecipare_gruppi_gruppoID` FOREIGN KEY (`gruppoID`) REFERENCES `gruppi` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `partecipare_gruppi_utenteUsername` FOREIGN KEY (`utenteUsername`) REFERENCES `utenti` (`userName`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limiti per la tabella `ricevere_msg_gruppi`
--
ALTER TABLE `ricevere_msg_gruppi`
  ADD CONSTRAINT `ricevere_msg_gruppi_destinatarioUsername` FOREIGN KEY (`destinatarioUsername`) REFERENCES `utenti` (`userName`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `ricevere_msg_gruppi_gruppoID` FOREIGN KEY (`chattare_gruppi_ID`) REFERENCES `chattare_gruppi` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limiti per la tabella `richieste_amicizia`
--
ALTER TABLE `richieste_amicizia`
  ADD CONSTRAINT `richieste_amicizia_nickDestinatario` FOREIGN KEY (`nickDestinatario`) REFERENCES `utenti` (`userName`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `richieste_amicizia_nickMittente` FOREIGN KEY (`nickMittente`) REFERENCES `utenti` (`userName`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
