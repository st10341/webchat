<?php

    // Avvio della sessione
    session_start();
    

    require_once $_SESSION['pathRequire'] . "/PHP/DMO/utente.inc.php";
    require_once $_SESSION['pathRequire'] . "/PHP/BL/UTENTE/utente_metodi.php";


    // Controllo preliminare
    if ($_SERVER["REQUEST_METHOD"] == "POST") 
    {
        // Ottieni il corpo della richiesta
        $json = file_get_contents('php://input');

        // Decodifica il JSON in un array associativo
        $dati = json_decode($json, true);

        if ($dati["azione"] == "login") 
        {
            // Effettua il login
            $response = effettuaLogin($dati);
            echo json_encode($response);
        } 
        elseif ($dati['azione'] == "getAmici") 
        {
            // Acquisisci amici
            $amici = acquisiciAmici();
            echo json_encode($amici);
        } 
        elseif ($dati['azione'] == 'getMessaggi') 
        {
            // Acquisisci messaggi
            $response = acquisisciMessaggi($dati);
            echo json_encode($response);
        }
        elseif ($dati['azione'] == 'getRichiesteAmicizia')
        {
            // Acquisizione richieste di amicizia ricevute dell'utente loggato in sessione
            $response = acquisisciRichiesteAmicizia($dati);
            echo json_encode($response);
        }
    } else  echo "Richiesta non valida!";
    

    function effettuaLogin($dati) 
    {
        if (isset($dati["email"]) && isset($dati["password"]))
        {
            // Istanza della classe utenteORM
            $utente = new clsUtenteDMO();
            $utente->setEmail($dati["email"]);

            // Crittografia della password
            $password = md5($dati["password"]);
            $utente->setPassword($password);

            // Verifica le credenziali
            $corrispondenze = clsUtenteBL::effettuaLogin($utente->getEmail(), $utente->getPassword());

            if ($corrispondenze == 1) 
            {
                // Get del nickName dell'utente
                $nickName = clsUtenteBL::ricavaNickname($utente->getEmail());

                return array("success" => true, "message" => "Login effettuato con successo", 'nickUtente' => $nickName);
            } 
            else 
            {
                return array("success" => false, "message" => "Email o password errati!");
            }
        } 
        else array("success" => false, "message" => "Email e password richiesti!");
        
    }

    function acquisiciAmici() {
        // Richiama la funzione per acquisire con quali amici ha interagito l'utente loggato
        return clsUtenteBL::acquisiciAmici();
    }

    function acquisisciMessaggi($dati)
    {
        if (isset($dati["utente1"]) && isset($dati["utente2"])) {
            // Chiamata al metodo per salvare i messaggi scambiati tra i due utenti
            return clsUtenteBL::acquisisciMessaggi($dati["utente1"], $dati["utente2"]);
        } else {
            return array("success" => false, "message" => "Utenti non specificati!");
        }
    }

    function acquisisciRichiesteAmicizia($dati)
    {
        if(isset($dati['utente']))
        {
            //creo il vettore per inserire le richieste
            $richieste = array();
            $destinatario = $dati['utente'];

            //dato il nickName di un utente, mi acquisisco tutte le richieste che gli sono arrivate oppure quelle inviate

            //acquisisco le richieste che sono arrivate a quell'utente
            $res = Database::eseguiSelect("richieste_amicizia", "nickMittente, orarioInvio", "nickDestinatario = '$destinatario'", "orarioAccettata = '2000-01-01 00:00:00'", "AND");

            //ciclo sui risultati
            foreach ($res as $risultato)
            {
                //istanzio la classe delle variabili
                $variabili = new clsRichiesteAmiciziaDMO();

                //mi salvo i valori
                $variabili->setOrarioInvio($risultato['orarioInvio']);
                $variabili->setNickMittente($risultato['nickMittente']);

                //inserisco i valori nel vettore
                $richieste[] = $variabili;
            }

            //restituisco il vettore
            return $richieste;
        }
        
    }


?>