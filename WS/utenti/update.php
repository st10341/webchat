<?php

    // Avvio della sessione
    session_start();
        
    require_once $_SESSION['pathRequire'] . "/PHP/DMO/utente.inc.php";
    require_once $_SESSION['pathRequire'] . "/PHP/BL/UTENTE/utente_metodi.php";
    require_once $_SESSION['pathRequire'] . "/PHP/DMO/richieste_amicizia.inc.php";


    // Controllo preliminare
    if ($_SERVER["REQUEST_METHOD"] == "POST") 
    {
        // Ottieni il corpo della richiesta
        $json = file_get_contents('php://input');

        // Decodifica il JSON in un array associativo
        $dati = json_decode($json, true);

        if ($dati["azione"] == "accettaRichiesta") 
        {
            // Effettua il login
            $response = accettaRichiestaAmicizia($dati);
            echo json_encode($response);
        }
    } else  echo "Richiesta non valida!";

    function accettaRichiestaAmicizia($dati)
    {
        if(isset($dati['mittente']) && isset($dati['destinatario']) && isset($dati['orarioAccettata']))
        {
            // Istanza la classe richiesteAmiciziaDMO
            $richieste = new clsRichiesteAmiciziaDMO();

            // Set proprietà
            $richieste->setNickMittente($dati['mittente']);
            $richieste->setNickDestinatario($dati['destinatario']);
            $richieste->setOrarioAccettata($dati['orarioAccettata']);

            // Update nel database
            $res = clsUtenteBL::accettaRichiesta($richieste->getNickMittente(), $richieste->getNickDestinatario(), $richieste->getOrarioAccettata());

            if ($res == 1) // Richiesta accettata con successo
            {
                return array("success" => true, "message" => "Richiesta accettata con successo!");
            }
            elseif ($res == 0)  // Accettazione richiesta non andata a buon fine
            {
                return array("success" => false, "message" => "Accettazione richiesta non andata a buon fine!");
            }
        }
    }
?>