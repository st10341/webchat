<?php

    // Avvio della sessione
    session_start();

    // Include la classe utenteDMO
    require_once $_SESSION['pathRequire'] . '/PHP/DMO/utente.inc.php';

    // Include la classe chattareUtenteDMO
    require_once $_SESSION['pathRequire'] . '/PHP/DMO/chattare_utente.inc.php';

    // Include la classe richieste_amiciziaDMO
    require_once $_SESSION['pathRequire'] . '/PHP/DMO/richieste_amicizia.inc.php';

    // Include la classe utenteBL
    require_once $_SESSION['pathRequire'] . '/PHP/BL/UTENTE/utente_metodi.php';
    


    // Controllo preliminare
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        // Ottieni il corpo della richiesta
        $json = file_get_contents('php://input');

        // Decodifica il JSON in un array associativo
        $dati = json_decode($json, true);


        if ($dati["azione"] == "register") // Register
        {
            // Effettua registrazione
            $response = effettuaRegister($dati);
            echo json_encode($response);
        }
        elseif($dati["azione"] == "inviaMsg")
        {
            $response = sendMsg($dati);
            echo json_encode($response);
        }
        elseif($dati['azione'] == 'inviaRichiesta') //Invio richiesta amicizia
        {
            $response = inviaRichiesta($dati);
            
            echo json_encode($response);
        }
    }


    function effettuaRegister($dati)
    {
        if (isset($dati["nome"]) && isset($dati["cognome"]) && isset($dati["dataNascita"]) && isset($dati["userName"]) && isset($dati["email"]) && isset($dati["password"]))  // Check campi compilati
        {
            // Istanza della classe utenteORM
            $utente = new clsUtenteDMO();


            // Set delle proprietà dell'utente
            $utente->setNome($dati["nome"]);
            $utente->setCognome($dati["cognome"]);
            $utente->setDataNascita($dati["dataNascita"]);
            $utente->setUserName($dati["userName"]);
            $utente->setEmail($dati["email"]);
            $utente->setPassword($dati["password"]);


            // Crittografia della password
            $psw = md5($utente->getPassword());


            // Risalvo la password nella classe
            $utente->setPassword($psw);
            
            
            // Richiama il metodo per effettuare la insert dell'utente
            $res = clsUtenteBL::effettuaRegister($utente->getNome(), $utente->getCognome(), $utente->getDataNascita(), $utente->getUserName(), $utente->getEmail(), $utente->getPassword());

            // Check eventuali errori
            if ($res == "email")    // Email già esistente
            {
                return array("success" => false, "message" => "Email già esistente");
            }
            elseif ($res == "username") // UserName già esistente
            {
                return array("success" => false, "message" => "Username già esistente");
            }
            else    // Utente inserito con successo
            {
                return array("success" => true, "nickUtente" => "$res");
            }
        } 
    }

    function sendMsg($dati)
    {
        if (isset($dati["messaggio"]) && isset($dati["orarioInvio"]) && isset($dati["stato"]) && isset($dati["mittente"]) && isset($dati["destinatario"]))
        {
            //istanzio la classe per gestire l'invio dei messaggi tra utenti
            $messaggi = new clsChattareUtenteDMO();


            //mi salvo i valori inseriti
            $messaggi->setMessaggio($dati['messaggio']);
            $messaggi->setOrarioInvio($dati['orarioInvio']);
            $messaggi->setStato($dati['stato']);
            $messaggi->setMittente($dati['mittente']);
            $messaggi->setDestinatario($dati['destinatario']);


            if ($messaggi->getMessaggio() != "")
            {
                //richiamo il metodo per inviare il messaggio
                $esito = clsUtenteBL::inviaMessaggio($messaggi->getMessaggio(), $messaggi->getOrarioInvio(), $messaggi->getStato(), $messaggi->getMittente(), $messaggi->getDestinatario());
                
                if ($esito == 1)
                {
                    return array("success" => true, "message" => "Messaggio inserito");
                }
                else
                {
                    return array("success" => false, "message" => "Messaggio non inserito!");
                }
            }
            else header("Location: /webchat/PHP/CHAT/chat.php");
        }
    }

    function inviaRichiesta($dati)
    {
        if(isset($dati['orarioInvio']) && isset($dati['mittente']) && isset($dati['destinatario']))
        {
            //istanzio la classe delle variabili delle richieste di amicizia
            $richiesteAmic = new clsRichiesteAmiciziaDMO();

            //salvo tutte le variabili
            $richiesteAmic->setOrarioInvio($dati['orarioInvio']);
            $richiesteAmic->setNickMittente($dati['mittente']);
            $richiesteAmic->setNickDestinatario($dati['destinatario']);

            // Check che l'utente inserito non sia il proprio
            if ($richiesteAmic->getNickMittente() == $richiesteAmic->getNickDestinatario())
            {
                return array("success" => false, "message" => "Non puoi inviare una richiesta di amicizia a te stesso!");

                /*
                echo "qui";
                header("Location: /webchat/PHP/RICHIESTA_AMICIZIA/INVIA_RICHIESTA/inviaRichiesta.php?error=Non puoi inviare una richiesta di amicizia a te stesso!");*/
            }
            else
            {
                // Check che l'utente inserito esista nel db
                $res = clsUtenteBL::verificaEsistenzaUtente($richiesteAmic->getNickDestinatario());

                
                if ($res > 0) // Utente esistente
                {
                    // Check se l'utente loggato abbia già inviato una richiesta di amicizia al destinatario
                    $res = clsUtenteBL::verificaEsistenzaRichiesta($richiesteAmic->getNickDestinatario(), $richiesteAmic->getNickMittente());

                    
                    if ($res == 1) 
                    {
                        return array("success" => false, "message" => "Hai gia inviato una richiesta di amicizia a questo utente!");

                        // header("Location: /webchat/PHP/RICHIESTA_AMICIZIA/INVIA_RICHIESTA/inviaRichiesta.php?error=Hai gia inviato una richiesta di amicizia"); 
                    }
                    else
                    {
                        //richiamo il metodo per inviare la richiesta
                        $esito = clsUtenteBL::inviaRichiesta($richiesteAmic->getOrarioInvio(), $richiesteAmic->getNickMittente(), $richiesteAmic->getNickDestinatario());

                        if ($esito == 1)    // Richiesta inviata al db
                        {
                            return array("success" => true, "message" => "Richiesta inviata con successo!");
                        }
                        elseif($esito == 0) // Erroro inserimento nel db
                        {
                            return array("success" => false, "message" => "Errore invio richiesta amicizia!");
                        }
                        elseif($esito == "Utente non trovato!") // Utente inesistente
                        {
                            return array("success" => false, "message" => "Utente inesistente!");
                        }
                        
                    }
                    
                }
                else return array("success" => false, "message" => "Utente inesistente!");
                
                
            }
        }
    }

?>