var timer = false;

function aggiungiUtente()
{

    // Effettua una richiesta AJAX per ottenere il contenuto della pagina PHP
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var nuovoDiv = document.createElement('div');
            nuovoDiv.innerHTML = xhr.responseText;
            document.getElementById('contatti').appendChild(nuovoDiv);
        }
    };
    xhr.open('GET', '/webchat/TEMPL/CONTATTO/contatto.php', true);
    xhr.send();

}

function aggiungiMessaggi() {
    // Effettua una richiesta AJAX per ottenere il contenuto della pagina PHP
    var xhr = new XMLHttpRequest();
    xhr.open('GET', '/webchat/TEMPL/MESSAGGIO/messaggio.php', true);
    xhr.send();
    xhr.onreadystatechange = function() 
    {
        if (xhr.readyState === 4 && xhr.status === 200) 
        {
            var nuovoDiv = document.createElement('div'); // Dichiarato fuori dalla condizione if-else
            if (timer) 
            {
                var elementoDaRimuovere = document.getElementById('div');
                if (elementoDaRimuovere) 
                {
                    elementoDaRimuovere.remove();
                }
            }
            nuovoDiv.innerHTML = this.responseText;
            nuovoDiv.id = 'div'; // Assegna un id al nuovo div
            document.getElementById('chat').appendChild(nuovoDiv);

            // Dopo aver aggiunto il nuovo messaggio, scorrere il div della chat verso il basso
            var chatDiv = document.getElementById('chat');
            chatDiv.scrollTop = chatDiv.scrollHeight;
        }
    };

    // Avvia il timer con un tempo di 5 secondi
    avviaTimer(5);
}

function avviaTimer(tempoInSecondi) 
{
    setTimeout(function() {
        timer = true;
        // Quando il timer scade, esegui l'azione AJAX
        aggiungiMessaggi();
    }, tempoInSecondi * 1000); // Converti i secondi in millisecondi
}



function redirectToNewChatPage() {
    window.location.href = "/webchat/PHP/SCEGLI_AMICO/scegliAmico.php";
}

function redirectToNewGroupPage() {
    window.location.href = "pagina_di_destinazione_per_nuovo_gruppo.php";
}
