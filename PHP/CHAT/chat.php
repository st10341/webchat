<?php

    include_once '../global.php';

    session_start();
    $pathRequire = $_SESSION['pathRequire'];
    $nickUtente = $_SESSION['nickUtente'];

    // Verifica se l'utente è loggato, altrimenti reindirizza alla pagina di login
    if (!isset($_SESSION['nickUtente']) || empty($_SESSION['nickUtente'])) 
    {
        header("Location: /webchat/index.php");
        exit(); // Assicura che lo script si interrompa dopo il reindirizzamento
    }

    if (isset($_SESSION['amicoCliccato']))  // È stato premuto un utente dalla lista quindi viene richiamato il metodo AJAX per generare i messaggi 
    {
        //metto su una variabile locale il nickName dell'amico cliccato
        $amicoCliccato = $_SESSION['amicoCliccato'];

        echo '<script src="chat.js"></script>';
        echo '<script>aggiungiMessaggi();</script>';
    }

    if (isset($_SESSION['esitoRichiesta']))
    {
        $esitoRichiesta = $_SESSION['esitoRichiesta'];
        $ok = $_SESSION['ok'];
        unset($_SESSION['esitoRichiesta']);
        unset($_SESSION['ok']);
    }

    if (isset($_SESSION['esitoAccettazione']))
    {
        $esitoRichiesta = $_SESSION['esitoAccettazione'];
        $ok = $_SESSION['ok'];
        unset($_SESSION['esitoAccettazione']);
        unset($_SESSION['ok']);
    }

?>


<!DOCTYPE html>
<html lang="it">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>CHAT WAVE</title>

        <!-- Latest compiled and minified CSS  -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- Latest compiled JavaScript -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>

        <link rel="stylesheet" href="chat.css">
        <link rel="stylesheet" href="/webchat/PHP/global.css">

        <script src="chat.js"></script>

    </head>

    <body onload="aggiungiUtente()">

        <?php include $pathRequire . '/TEMPL/HEADER/header.php'; ?> 
    
        <?php if(isset($esitoRichiesta)): ?>

            <?php if($ok == "true"): ?>
                <div id="errore" style="background-color: green;">
                    <p><?php echo $esitoRichiesta; ?></p>
                </div>
            <?php elseif($ok == "false"): ?>
                <div id="errore" style="background-color: red;">
                    <p><?php echo $esitoRichiesta; ?></p>
                </div>
            <?php endif; ?>

        <?php endif; ?>


        <div class="row" id="contattiChat">

                
            <div class="col-3" style="padding-left: 0">

                <div id="bottoni" class="rounded-top-4 p-3" style="margin-bottom: 0px">
                    <div>

                        <div class="col" style="padding-bottom: 10px;">
                            <input class="rounded-3 btn" type="submit" value="NUOVA CHAT" onclick="redirectToNewChatPage()">
                        </div>

                        <div class="col" style="padding: 0px;">
                            <input class="rounded-3 btn" type="submit" value="NUOVO GRUPPO" onclick="redirectToNewGroupPage()">
                        </div>

                    </div>
                </div>

                <div id="contatti" class="rounded-bottom-4 p-3 m-0"></div>

            </div>


            <div class="col-9 p-0" id="divDestra">

                <?php if(isset($amicoCliccato)): ?>

                    <div class="rounded-top-4 p-3 infContatto">
                        <p class="m-0"><?php echo $amicoCliccato ?></p>
                    </div>


                <?php else: ?>

                    <div class="rounded-top-4 p-3 infContatto">
                        <p class="m-0">‎ </p> <!-- BARBATRUCCO -->
                    </div>


                    <div id="alert">
                        <h1>Seleziona un utente dalla lista oppure crea un nuovo gruppo</h1>
                    </div>
                    
                <?php endif; ?>

                <div id="chat" class="p-3 chat"></div>

                <div>
                    <form method="POST" action="/webchat/PHP/BL/UTENTE/utentePostBack.php">

                        <div class="invioMessaggio rounded-bottom-4">
                            
                            <input name="inviaFile" style="display:none;"></input>

                            <div class="btns" onclick="document.getElementsByName('inviaMsg')[0].click()">
                                <img src="/webchat/IMG/paper-clip.png">
                            </div>
                            

                            <input name="messaggio" class="input" type="text" placeholder="Scrivi il tuo messaggio">
                            

                            <input type="submit" name="inviaMsg" class="btns" value="" style="display:none;">

                            <div class="btns" onclick="document.getElementsByName('inviaMsg')[0].click()">
                                <img src="/webchat/IMG/send-message.png" style="margin-left: 3px;">
                            </div>
                            
                        </div>

                    </form>
                </div>

            </div>

                
        </div>


        <?php include $pathRequire . '/TEMPL/FOOTER/footer.php'; ?> 
        

    </body>

</html>