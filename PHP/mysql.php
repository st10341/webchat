<?php
// Disabilita la visualizzazione dei notice
error_reporting(E_ALL & ~E_NOTICE);
    class Database{

        private static $conn;

        // Costruttore per creare una connessione al database
        public static function initialize() 
        {
            /*
            // Dati di connessione al database remoto
            $host = 'localhost';
            $dbName = 'ui1evo7i_webchat';
            $user = 'ui1evo7i_webchat';
            $password = '9^{,6N0Gu"5{';
            */
        
            
            // Dati di connessione al database locale
            $host = 'localhost';
            $dbName = 'webchat';
            $user = 'root';
            $password = '';
            
            
            //connessione al DB
            self::$conn = new mysqli($host, $user, $password, $dbName);

            // Controlla la connessione
            if (self::$conn->connect_error) {
                die("Connessione fallita: " . self::$conn->connect_error);
            }
        }


        // Metodo per inserire dati in una tabella specificando il nome della tabella e i parametri
        public static function insertIntoTable($tableName, $params) 
        {
            Database::initialize();

            // query SQL
            $columns = implode(", ", array_keys($params));  //separa i parametri dalla virgola
            $values = "'" . implode("', '", array_values($params)) . "'";   //separa i valori dalla virgola
            $sql = "INSERT INTO $tableName ($columns) VALUES ($values)";


            // Esegui la query
            if (self::$conn->query($sql) === TRUE) return 1;
            else return 0;
            
        }


        // Metodo per aggiornare dati in una tabella specificando il nome della tabella, i parametri e l'operatore logico
        public static function updateTable($tableName, $params, $condition1, $condition2, $operator) 
        {
            Database::initialize();

            // Costruzione della parte SET della query SQL
            $setValues = "";
            foreach ($params as $column => $value) {
                $setValues .= "$column = '$value', ";
            }
            $setValues = rtrim($setValues, ", ");


            // Costruzione della query
            $sql = "UPDATE $tableName SET $setValues WHERE $condition1";


            //se ho inserito un'operatore logico lo aggiungo alla query
            if ($operator && ($operator == "AND" || $operator == "OR")) {
                $sql .= " $operator $condition2";
            }


            // Esegui la query
            if (self::$conn->query($sql) === TRUE) return 1;
            else return 0;
            
        }
        

        
        // Metodo per eliminare dati da una tabella
        public static function deleteTable($tableName, $condition) 
        {
            Database::initialize();

            // Costruzione della query
            $sql = "DELETE FROM $tableName WHERE $condition";


            // Esegui la query
            if (self::$conn->query($sql) === TRUE) echo "Record cancellato con successo";
            else echo "Errore durante la cancellazione del record: " . self::$conn->error;
            
        }
        

        // Metodo per eseguire una query di count specificando una sola condizione
        public static function countRowsONE($tableName, $condition) 
        {
            Database::initialize();

            // Costruzione della query
            $sql = "SELECT COUNT(*) as count FROM $tableName WHERE $condition";

            // Esegui la query
            $result = self::$conn->query($sql);
            
            //echo $sql;
            
            // Verifica se la query è stata eseguita correttamente
            if ($result !== FALSE) 
            {
                // Recupera il risultato della query
                $row = $result->fetch_assoc();
                
                // Estrai il valore del conteggio
                $count = $row['count'];
                
                // Ritorna il conteggio delle righe
                return $count;
            }
            else
            {
                // Gestisci eventuali errori nell'esecuzione della query
                echo "Errore durante l'esecuzione della query: " . self::$conn->error;
                return -1; // Ritorna un valore di errore
            }
            
        }


        // Metodo per eseguire una query di count specificando due condizioni e un operatore logico
        public static function countRowsTWO($tableName, $condition1, $condition2, $operator) 
        {
            Database::initialize();

            // Costruzione della query
            $sql = "SELECT COUNT(*) as count FROM $tableName WHERE $condition1";
        
            

            // Se è specificato un operatore, lo aggiungiamo alla query
            if ($operator && ($operator == "AND" || $operator == "OR")) {
                $sql .= " $operator $condition2";
            }
        
            //echo $sql;

            // Esegui la query
            $result = self::$conn->query($sql);
            

            // Verifica se la query è stata eseguita correttamente
            if ($result !== FALSE) 
            {
                // Recupera il risultato della query
                $row = $result->fetch_assoc();
                
                // Estrai il valore del conteggio
                $count = $row['count'];
                
                // Ritorna il conteggio delle righe
                return $count;
            }
            else
            {
                // Gestisci eventuali errori nell'esecuzione della query
                echo "Errore durante l'esecuzione della query: " . self::$conn->error;
                return -1; // Ritorna un valore di errore
            }
        
        }  
        
        // Metodo per eseguire una query di selezione sul database e restituire i risultati
        public static function eseguiSelect($tableName, $columns, $condition1, $condition2, $operator) 
        {
            Database::initialize();

            // Costruizione della query
            $sql = "SELECT $columns FROM $tableName";
            if (!empty($condition1)) {
                $sql .= " WHERE $condition1";
            }

            
            // Se è specificato un operatore, lo aggiungiamo alla query
            if ($operator && ($operator == "AND" || $operator == "OR")) {
                $sql .= " $operator" . "(" . "$condition2" . ")";
            }
            

            // Esegui la query
            $result = self::$conn->query($sql);


            // Verifica se la query ha prodotto un errore
            if (!$result) {
                die("Errore nella query: " . self::$conn->error);
            }


            // Inizializza un array per memorizzare i risultati della query
            $risultati = array();


            // Itera sui risultati della query e li aggiunge all'array
            while ($row = mysqli_fetch_assoc($result)) {
                $risultati[] = $row;
            }

            // Libera la memoria associata al risultato
            mysqli_free_result($result);

            return $risultati;

        }

        public static function eseguiSelectCustom($sql)
        {
            Database::initialize();
            
            // Esegui la query
            $result = self::$conn->query($sql);

            // Verifica se la query ha prodotto un errore
            if (!$result) {
                die("Errore nella query: " . self::$conn->error);
            }


            // Inizializza un array per memorizzare i risultati della query
            $risultati = array();


            // Itera sui risultati della query e li aggiunge all'array
            while ($row = mysqli_fetch_assoc($result)) {
                $risultati[] = $row;
            }

            // Libera la memoria associata al risultato
            mysqli_free_result($result);

            return $risultati;
        }
    }


?>
