<?php

    //session_start();
    $pathRequire = $_SESSION['pathRequire'];
    session_write_close();

    //echo $pathIndex;
    
    require_once $pathRequire . '/PHP/mysql.php';


    class clsUtenteBL
    {
        
        public static function effettuaLogin($email, $password){

            Database::initialize();
            //innanzi tutto devo verificare che l'email e la password inseriti siano corretti
            $corrispondenze = Database::countRowsTWO("utenti", "email = '$email'", "password = '$password'", "AND");
            if ($corrispondenze > 0) return 1; //le credenziali sono corrette  
            else return 0;  //le credenziali sono errate
            
        }

        public static function ricavaNickname($email){

            //effettuo una select e restituisco il nickName in base alla email fornita
            $res = Database::eseguiSelect("utenti", "userName", "email = '$email'", "", "");

            //mi salvo il nickName su una variabile
            foreach ($res as $risultato) {
                $nickname = $risultato['userName'];
            }

            //restituisco il nick
            return $nickname;
        }

        public static function controllaNick($utente){
            //verificare che il nickname inserito esista
            $corrispondenze = Database::countRowsONE('utenti', "userName = '$utente'");
            
            if ($corrispondenze > 0) return true;
            else return false;
        }

        public static function effettuaRegister($nome, $cognome, $dataNascita, $userName, $email, $password){

            //inanzi tutto devo verificare che email e userName non siano gia esistenti
            $res = Database::countRowsTWO('utenti', "email = '$email'", "userName = '$userName'", 'OR');

            if ($res > 0)   //email o userName già esistenti
            {
                //faccio un'ulteriore verifica per vedere se è presente email o password
                $res = Database::countRowsONE('utenti', "email = '$email'"); //controllo se è l'email già esistente

                if ($res > 0) return "email";
                else return "username";
            }
            else{
                //posso fare la insert

                // Popolo il vettore dei parametri
                $data = array(
                    'nome' => $nome,
                    'cognome' => $cognome,
                    'dataNascita' => $dataNascita,
                    'username' => $userName,
                    'email' => $email,
                    'password' => $password
                );

                Database::insertIntoTable('utenti', $data);
                return "$userName";
            }
        }

        public static function inviaMessaggio($messaggio, $orarioInvio, $stato, $mittente, $destinatario)
        {
            //popolo l'array
            $data = array(
                'messaggio' => $messaggio,
                'orarioInvio' => $orarioInvio,
                'stato' => $stato,
                'mittenteUsername' => $mittente,
                'destinatarioUsername' => $destinatario
            );
            
            //effettuo la insert
            $esito = Database::insertIntoTable('chattare_utenti', $data);

            return $esito;

        }

        public static function acquisisciMessaggi($utente1, $utente2)
        {

            //creo il vettore per inserire le richieste
            $messaggi = array();

            /*
            SELECT messaggio, mittenteUsername, destinatarioUsername 
            FROM `chattare_utenti` 
            WHERE (mittenteUsername = "Maury4828" OR destinatarioUsername = "Maury4828") AND (destinatarioUsername = "luigimarroni" OR mittenteUsername = "luigimarroni")
            */

            //acquisisco i messaggi scambiati tra i due utenti
            $res = Database::eseguiSelectCustom(
                "SELECT messaggio, mittenteUsername, destinatarioUsername 
                FROM chattare_utenti
                WHERE (mittenteUsername = '$utente1' OR destinatarioUsername = '$utente1') AND (destinatarioUsername = '$utente2' OR mittenteUsername = '$utente2')");

            //ciclo sui risultati
            foreach ($res as $risultato)
            {
                //mi salvo i risultati della query
                $messaggio = $risultato['messaggio'];
                $nickMittente = $risultato['mittenteUsername'];
                $nickDestinatario = $risultato['destinatarioUsername'];

                $messaggi[] = array($messaggio, $nickMittente, $nickDestinatario);
            }

            //restituisco il vettore
            return $messaggi;
        }

        public static function acquisiciAmici()
        {
            //creo il vettore per inserire le richieste
            $utenti = array();

            //mi salvo in locale il nickName loggato
            $username = $_SESSION['nickUtente'];

            //acquisisco gli utenti con cui l'utente ha interagito, oppure gli utenti che hanno interagito con lui
            $res = Database::eseguiSelectCustom(
            "SELECT DISTINCT LEAST(mittenteUsername, destinatarioUsername) AS utente1, 
            GREATEST(mittenteUsername, destinatarioUsername) AS utente2 
            FROM chattare_utenti 
            WHERE mittenteUsername = '$username' OR destinatarioUsername = '$username'");

            //ciclo sui risultati
            foreach ($res as $risultato)
            {
                //mi salvo il nickName del mittente e del destinatario della richiesta di amicizia
                $nickMittente = $risultato['utente1'];
                $nickDestinatario = $risultato['utente2'];

                if ($nickMittente == $username) $utenti[] = $nickDestinatario;
                else $utenti[] = $nickMittente;
            }

            //restituisco il vettore
            return $utenti;
        }

        public static function inviaRichiesta($orarioInvio, $mittente, $destinatario)
        {
            //controllo se il nickName inserito esista nel db
            $conteggio = Database::countRowsONE('utenti', "userName = '$destinatario'");


            if($conteggio > 0)  //ha trovato il nickname inserito
            {
                //popolo l'array
                $data = array(
                    'orarioInvio' => $orarioInvio,
                    'nickMittente' => $mittente,
                    'nickDestinatario' => $destinatario
                );

                //effettuo la insert
                $esito = Database::insertIntoTable('richieste_amicizia', $data);

                return $esito;
            }
            else return "Utente non trovato!";
            
            
        }

        public static function verificaEsistenzaRichiesta($destinatario, $mittente)
        {
            //eseguo il count per verificare se è stata gia inviata una richiesta di amicizia all'utente digitato
            $res = Database::countRowsTWO("richieste_amicizia", "nickDestinatario = '$destinatario'", "nickMittente = '$mittente'", "AND"); 


            if ($res > 0) return true;  //è stata inviata una richiesta
            else return false;  //nuova richiesta
        }

        public static function verificaEsistenzaUtente($destinatario)
        {
            return $res = Database::countRowsONE('utenti', "userName = '$destinatario'");
        }
        
        public static function acquisisciRichiesteAmic($destinatario)
        {
            //creo il vettore per inserire le richieste
            $richieste = array();

            //dato il nickName di un utente, mi acquisisco tutte le richieste che gli sono arrivate oppure quelle inviate

            //acquisisco le richieste che sono arrivate a quell'utente
            $res = Database::eseguiSelect("richieste_amicizia", "nickDestinatario", "nickDestinatario = '$destinatario'", "","");

            //ciclo sui risultati
            foreach ($res as $risultato) 
            {
                //istanzio la classe delle variabili
                $variabili = new clsRichiesteAmiciziaDMO();

                //mi salvo i valori
                $variabili->setOrarioInvio($risultato['orarioInvio']);
                $variabili->setNickMittente($risultato['nickMittente']);

                //inserisco i valori nel vettore
                $richieste[] = $variabili;
            }

            //restituisco il vettore
            return $richieste;
        }

        public static function accettaRichiesta($mittente, $destinatario, $orarioAccettata)
        {
            
            //devo aggiornare la richiesta già presente nel DB aggiungendo l'orario in cui è stata accettata la richiesta così in questo modo i due utenti possono comunicare tra di loro
            $count = Database::updateTable('richieste_amicizia', ['orarioAccettata' => $orarioAccettata], "nickMittente = '$mittente'", "nickDestinatario = '$destinatario'", "AND");

            return $count;
        }
    }


?>