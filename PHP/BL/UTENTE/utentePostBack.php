<?php
    session_start();
    $pathRequire = $_SESSION['pathRequire'];
    $pathWS = $_SESSION['pathWS'];
    session_write_close();

    //includi la classe utente_variabili
    require_once $pathRequire . '/PHP/DMO/utente.inc.php';

    //includi la classe chattare_utente variabili
    include_once $pathRequire .  '/PHP/DMO/chattare_utente.inc.php';

    // Include la classe richieste_amicizia variabili
    include_once $pathRequire . '/PHP/DMO/richieste_amicizia.inc.php';

    //include i metodi
    require_once 'utente_metodi.php';


    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {

        if (file_get_contents('php://input'))
        {
            // Ottieni il corpo della richiesta
            $json = file_get_contents('php://input');

            // Decodifica il JSON in un array associativo
            $dati = json_decode($json, true);

            
        }

        if (isset($_POST['azione'])) //Register, Login, recuperoPsw, resetPsw, InviaRichiestaAmicizia, accettaRichiestaAmicizia
        {
            
            if ($_POST['azione'] == 'SIGN-UP') //REGISTRAZIONE - //WS
                register($pathWS);
            elseif ($_POST['azione'] == 'LOGIN')    //LOGIN - WS
                login($pathWS);
            elseif ($_POST['azione'] == 'RECUPERA') //recuperoPsw
                recuperoPsw($pathWS);
            elseif ($_POST['azione'] == 'AVANTI')   //resetPsw
                resetPsw($pathWS);
            elseif ($_POST['azione'] == 'INVIA')    // Invia richiesta amicizia - WS
                inviaRichiestaAmicizia($pathWS);
            
        }
        elseif (isset($_POST['inviaMsg']))  // È stato premuto il tasto per inviare un nuovo messaggio
            inviaNuovoMessaggio($pathWS);
        elseif (isset($_POST['accetta']))
            accettaRichiesta($pathWS);
        elseif (isset($_POST['rifiuta']))
            rifiutaRichiesta($pathWS);
        elseif(isset($dati['richiesta']))   // Richieste arrivate direttamente da pagine php, senza la pressione di bottoni
        {
            if ($dati['richiesta'] == 'getMsg')    // Acquisizione dei messaggi scambiati tra due utenti
                acquisisciMessaggiScambiati($dati, $pathWS);
            elseif ($dati['richiesta'] == 'acquisisciRichiesteAmic')
            {
                acquisisciRichiesteAmic($dati, $pathWS);
            }
                
        }
    }


    function register($pathWS)
    {
        //istanzio la classe utente_variabili
        $utente = new clsUtenteDMO();

                        
        //acquisisco i dati dalla form di registrazione
        $utente->setNome($_POST['RegNome']);
        $utente->setCognome($_POST['RegCognome']);
        $utente->setDataNascita($_POST['RegDataNascita']);
        $utente->setUserName($_POST['RegUserName']);
        $utente->setEmail($_POST['RegEmail']);
        $utente->setPassword($_POST['RegPassword']);


        // URL del web service
        $url = 'http://' . $pathWS . '/webchat/WS/utenti/create.php';

        // Dati da inviare al web service in formato JSON
        $postData = json_encode(array(
            'azione' => 'register',
            'nome' => $utente->getNome(),
            'cognome' => $utente->getCognome(),
            'dataNascita' => $utente->getDataNascita(),
            'userName' => $utente->getUserName(),
            'email' => $utente->getEmail(),
            'password' => $utente->getPassword()
        ));


        // Inizializza una sessione cURL
        $ch = curl_init($url);


        // Imposta le opzioni della richiesta cURL
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($postData)
        ));

        // Ottieni i cookies della sessione
        $sessionCookies = session_id();

        // Imposta l'opzione CURLOPT_COOKIE per includere i cookies nella richiesta cURL
        curl_setopt($ch, CURLOPT_COOKIE, "PHPSESSID=" . $sessionCookies);


        // Esegui la richiesta e ottieni la risposta
        $response = curl_exec($ch);
        if ($response === false) 
        {
            // Gestisci il caso in cui non ci sia stata nessuna risposta
            echo 'Nessuna risposta dal server.';
        } 
        else 
        {
            // Debug della risposta ricevuta
            echo 'Risposta dal server: ' . $response;

            // Decodifica la risposta JSON
            $responseData = json_decode($response, true);
            //var_dump($responseData);

            // Gestisci la risposta del web service
            if ($responseData != "" || $responseData != NULL)
            {
                if ($responseData['success'] == false && $responseData['message'] == 'Email già esistente')
                {
                    // Redirect alla index
                    header("Location: ../../../index.php?errore=Email già esistente!");
                }
                elseif ($responseData['success'] == false && $responseData['message'] == 'Username già esistente')
                {
                    // Redirect alla index
                    header("Location: ../../../index.php?errore=Username già esistente!");
                }
                elseif ($responseData['success'] == true)
                {
                    // Set nella sessione corrente dell'utente loggato
                    session_start();
                    $_SESSION['nickUtente'] = $responseData['nickUtente'];
                    session_write_close();

                    // Mette nei cookies del browser l'utente loggato
                    $expiry = time() + (30 * 24 * 60 * 60); // Scade tra 30 giorni
                    $path = '/'; // Accessibile in tutto il sito
                    $domain = $_SERVER['HTTP_HOST']; // Dominio corrente
                    $secure = isset($_SERVER['HTTPS']); // Solo via HTTPS se il server supporta HTTPS
                    $httponly = true; // Accessibile solo via HTTP

                    setcookie('utenteLoggato', $_SESSION['nickUtente'], $expiry, $path, $domain, $secure, $httponly);


                    // Redirect alla chat
                    header("Location: /webchat/PHP/CHAT/chat.php");
                }

            } else {
                // Gestisci il caso in cui la risposta non sia un JSON valido
                echo 'Errore: risposta non valida dal server.';
            }
        }

        // Chiudi la sessione cURL
        curl_close($ch);
    }

    function login($pathWS)
    {
        // Istanza della classe utenteORM
        $utente = new clsUtenteDMO();

        // Set dei valori inseriti
        $utente->setEmail($_POST['LogEmail']);
        $utente->setPassword($_POST['LogPassword']);

        
        // URL del web service
        $url = 'http://' . $pathWS . '/webchat/WS/utenti/read.php';
    
        // Dati da inviare al web service in formato JSON
        $postData = json_encode(array(
            'azione' => 'login',
            'email' => $utente->getEmail(),
            'password' => $utente->getPassword()
        ));

        
        // Inizializza una sessione cURL
        $ch = curl_init($url);


        // Imposta le opzioni della richiesta cURL
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($postData)
        ));

        // Ottieni i cookies della sessione
        $sessionCookies = session_id();

        // Imposta l'opzione CURLOPT_COOKIE per includere i cookies nella richiesta cURL
        curl_setopt($ch, CURLOPT_COOKIE, "PHPSESSID=" . $sessionCookies);


        // Esegui la richiesta e ottieni la risposta
        $response = curl_exec($ch);
        if ($response === false) {
            // Gestisci il caso in cui non ci sia stata nessuna risposta
            echo 'Nessuna risposta dal server.';
        } else {
            // Debug della risposta ricevuta
            echo 'Risposta dal server: ' . $response;

            // Decodifica la risposta JSON
            $responseData = json_decode($response, true);
            //var_dump($responseData);

            // Gestisci la risposta del web service
            if ($responseData != "" || $responseData != NULL)
            {
                if ($responseData['success'] == true)
                {
                    // Set nella sessione corrente dell'utente loggato
                    session_start();
                    $_SESSION['nickUtente'] = $responseData['nickUtente'];
                    session_write_close();

                    // Mette nei cookies del browser l'utente loggato
                    $expiry = time() + (30 * 24 * 60 * 60); // Scade tra 30 giorni
                    $path = '/'; // Accessibile in tutto il sito
                    $domain = $_SERVER['HTTP_HOST']; // Dominio corrente
                    $secure = isset($_SERVER['HTTP']); // Solo via HTTPS se il server supporta HTTPS
                    $httponly = true; // Accessibile solo via HTTP

                    setcookie('utenteLoggato', $_SESSION['nickUtente'], $expiry, $path, $domain, $secure, $httponly);


                    // Redirect alla chat
                    header("Location: /webchat/PHP/CHAT/chat.php");
                }
                elseif ($responseData['success'] == false)
                {
                    //Insert dell'errore in sessione
                    session_start();
                    $_SESSION['loginErr'] = $responseData['message'];
                    session_write_close();

                    // Redirect alla login
                    header("Location: /webchat/index.php");
                }

            } else {
                // Gestisci il caso in cui la risposta non sia un JSON valido
                echo 'Errore: risposta non valida dal server.';
            }
        }
        
        // Chiudi la sessione cURL
        curl_close($ch);
    }

    function recuperoPsw($pathWS)
    {
        //istanzio la classe utente_variabili
        $utente = new clsUtenteDMO();
        
        //mi salvo i valori inseriti
        $utente->setEmail($_POST['email']);

        //mi salvo l'email su una variabile locale
        $email = $utente->getEmail();
        
        //controllo che l'email inserita dall'utente per effettuare il recupero della password esista
        $corrispondenze = Database::countRowsONE('utenti', "email = '$email'");

        if ($corrispondenze > 0){
            session_start();
            //salvo l'email in sessione
            $_SESSION['email'] = $email;
            session_write_close();
            
            //indirizzamento pagina OTP
            header("Location: /webchat/PHP/RESET_PSW/invioOTP.php");
        }
        else header("Location: /webchat/PHP/RESET_PSW/recuperoPsw.php?errore=Email inesistente!");
    }

    function resetPsw($pathWS)
    {
        //istanzio la classe utente_variabili
        $utente = new clsUtenteDMO();

        //mi salvo la password
        $utente->setPassword($_POST['psw']);

        //mi salvo su delle variabili locali la password e l'email
        $email = $_SESSION['email'];
        $pass = $utente->getPassword();

        //crittografo la password
        $psw = md5($pass);


        // Effettuo la update
        $esito = Database::updateTable('utenti', ['password' => $psw], "email = '$email'", '', '');

        //reindirizzo alla form di login
        header("Location: /webchat/index.php");
    }

    function inviaRichiestaAmicizia($pathWS)
    {
        //istanzio la classe delle variabili delle richieste di amicizia
        $richiesteAmic = new clsRichiesteAmiciziaDMO();

        //salvo tutte le variabili
        $richiesteAmic->setOrarioInvio(date("Y-m-d H:i:s"));
        $richiesteAmic->setNickMittente($_SESSION['nickUtente']);
        $richiesteAmic->setNickDestinatario($_POST['nickDestinatario']);

        // URL del web service
        $url = 'http://' . $pathWS . '/webchat/WS/utenti/create.php';

        // Dati da inviare al web service in formato JSON
        $postData = json_encode(array(
            'azione' => 'inviaRichiesta',
            'orarioInvio' => $richiesteAmic->getOrarioInvio(),
            'mittente' => $richiesteAmic->getNickMittente(),
            'destinatario' => $richiesteAmic->getNickDestinatario()
        ));


        // Inizializza una sessione cURL
        $ch = curl_init($url);


        // Imposta le opzioni della richiesta cURL
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($postData)
        ));

        // Ottieni i cookies della sessione
        $sessionCookies = session_id();

        // Imposta l'opzione CURLOPT_COOKIE per includere i cookies nella richiesta cURL
        curl_setopt($ch, CURLOPT_COOKIE, "PHPSESSID=" . $sessionCookies);


        // Esegui la richiesta e ottieni la risposta
        $response = curl_exec($ch);

        if ($response === false) 
        {
            // Gestisci il caso in cui non ci sia stata nessuna risposta
            echo 'Nessuna risposta dal server.';
        } 
        else 
        {
            
            // Debug della risposta ricevuta
            //echo 'Risposta dal server: ' . $response;

            // Decodifica la risposta JSON
            $responseData = json_decode($response, true);
            //var_dump($responseData);
            // Gestisci la risposta del web service
            if ($responseData != "" || $responseData != NULL)
            {
                if ($responseData['success'] == true || $responseData['success'] == false)
                {
                    session_start();

                    $_SESSION['esitoRichiesta'] = $responseData['message'];

                    if ($responseData['success'] == true) $_SESSION['ok'] = "true";
                    elseif ($responseData['success'] == false) $_SESSION['ok'] = "false";
                    
                    session_write_close();

                    header("Location: /webchat/PHP/CHAT/chat.php");
                }


            } 
            else echo 'Errore: risposta non valida dal server.';
            
        }

        // Chiudi la sessione cURL
        curl_close($ch);
    }

    function inviaNuovoMessaggio($pathWS)
    {
        //istanzio la classe per gestire l'invio dei messaggi tra utenti
        $messaggi = new clsChattareUtenteDMO();


        //mi salvo i valori inseriti
        $messaggi->setMessaggio($_POST['messaggio']);
        $messaggi->setOrarioInvio(date("Y-m-d H:i:s"));
        $messaggi->setStato("S");
        $messaggi->setMittente($_SESSION['nickUtente']);
        $messaggi->setDestinatario($_SESSION['amicoCliccato']);


        // URL del web service
        $url = 'http://' . $pathWS . '/webchat/WS/utenti/create.php';

        // Dati da inviare al web service in formato JSON
        $postData = json_encode(array(
            'azione' => 'inviaMsg',
            'messaggio' => $messaggi->getMessaggio(),
            'orarioInvio' => $messaggi->getOrarioInvio(),
            'stato' => $messaggi->getStato(),
            'mittente' => $messaggi->getMittente(),
            'destinatario' => $messaggi->getDestinatario()
        ));


        // Inizializza una sessione cURL
        $ch = curl_init($url);


        // Imposta le opzioni della richiesta cURL
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($postData)
        ));

        // Ottieni i cookies della sessione
        $sessionCookies = session_id();

        // Imposta l'opzione CURLOPT_COOKIE per includere i cookies nella richiesta cURL
        curl_setopt($ch, CURLOPT_COOKIE, "PHPSESSID=" . $sessionCookies);


        // Esegui la richiesta e ottieni la risposta
        $response = curl_exec($ch);
        if ($response === false) 
        {
            // Gestisci il caso in cui non ci sia stata nessuna risposta
            echo 'Nessuna risposta dal server.';
        } 
        else 
        {
            // Debug della risposta ricevuta
            echo 'Risposta dal server: ' . $response;

            // Decodifica la risposta JSON
            $responseData = json_decode($response, true);
            

            // Gestisci la risposta del web service
            if ($responseData != "" || $responseData != NULL)
            {
                if ($responseData['success'] == true)
                {
                    // Redirect alla chat
                    header("Location: /webchat/PHP/CHAT/chat.php");
                }
                elseif ($responseData['success'] == false)
                {
                    // Redirect alla chat con errore
                    header("Location: /webchat/PHP/CHAT/chat.php?" . $responseData['message']);
                }

            } 
            else 
            {
                // Gestisci il caso in cui la risposta non sia un JSON valido
                echo 'Errore: risposta non valida dal server.';
            }
        }

        // Chiudi la sessione cURL
        curl_close($ch);
    }

    function acquisisciMessaggiScambiati($dati, $pathWS)
    {
        // URL del web service
        $url = 'http://' . $pathWS . '/webchat/WS/utenti/read.php';

        // Dati da inviare al web service in formato JSON
        $postData = json_encode(array(
            'azione' => 'getMessaggi',
            'utente1' => $dati['utente1'],
            'utente2' => $dati['utente2']
        ));


        // Inizializza una sessione cURL
        $ch = curl_init($url);


        // Imposta le opzioni della richiesta cURL
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($postData)
        ));

        // Ottieni i cookies della sessione
        $sessionCookies = session_id();

        // Imposta l'opzione CURLOPT_COOKIE per includere i cookies nella richiesta cURL
        curl_setopt($ch, CURLOPT_COOKIE, "PHPSESSID=" . $sessionCookies);


        // Esegui la richiesta e ottieni la risposta
        $response = curl_exec($ch);

        if ($response === false) 
        {
            // Gestisci il caso in cui non ci sia stata nessuna risposta
            echo 'Nessuna risposta dal server.';
        } 
        else 
        {
            // Debug della risposta ricevuta
            //echo 'Risposta dal server: ' . $response;

            // Decodifica la risposta JSON
            $responseData = json_decode($response, true);

            // Gestisci la risposta del web service
            if ($responseData != "" || $responseData != NULL)
            {
                // URL a cui vuoi inviare la richiesta POST
                $url = 'http://' . $pathWS . '/webchat/TEMPL/MESSAGGIO/messaggio.php';

                // Inizializza una sessione cURL
                $ch = curl_init($url);

                // Imposta le opzioni di cURL
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($responseData));
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen(json_encode($responseData))
                ));

                // Ottieni i cookies della sessione
                $sessionCookies = session_id();

                // Imposta l'opzione CURLOPT_COOKIE per includere i cookies nella richiesta cURL
                curl_setopt($ch, CURLOPT_COOKIE, "PHPSESSID=" . $sessionCookies);


                // Esegui la richiesta POST
                $postResponse = curl_exec($ch);

                // Controlla se ci sono errori nella richiesta
                if (curl_errno($ch)) 
                {
                    echo 'Errore cURL: ' . curl_error($ch);
                } 
                else 
                {
                    // Stampa la risposta dal server di destinazione
                    echo $postResponse;
                }

                // Chiudi la sessione cURL
                curl_close($ch);

            } 
            else 
            {
                // Gestisci il caso in cui la risposta non sia un JSON valido
                echo 'Errore: risposta non valida dal server.';
            }
        }

        // Chiudi la sessione cURL
        curl_close($ch);
    }

    function acquisisciRichiesteAmic($dati, $pathWS)
    {

        // URL del web service
        $url = 'http://' . $pathWS . '/webchat/WS/utenti/read.php';

        // Dati da inviare al web service in formato JSON
        $postData = json_encode(array(
            'azione' => 'getRichiesteAmicizia',
            'utente' => $dati['utente']
        ));


        // Inizializza una sessione cURL
        $ch = curl_init($url);


        // Imposta le opzioni della richiesta cURL
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($postData)
        ));

        // Ottieni i cookies della sessione
        $sessionCookies = session_id();

        // Imposta l'opzione CURLOPT_COOKIE per includere i cookies nella richiesta cURL
        curl_setopt($ch, CURLOPT_COOKIE, "PHPSESSID=" . $sessionCookies);


        // Esegui la richiesta e ottieni la risposta
        $response = curl_exec($ch);

        if ($response === false) 
        {
            // Gestisci il caso in cui non ci sia stata nessuna risposta
            echo 'Nessuna risposta dal server.';
        } 
        else 
        {
            // Debug della risposta ricevuta
            //echo 'Risposta dal server: ' . $response;

            // Decodifica la risposta JSON
            $responseData = json_decode($response, true);

            
            // Gestisci la risposta del web service
            if ($responseData != "" || $responseData != NULL)
            {
                // URL a cui vuoi inviare la richiesta POST
                $url = 'http://' . $pathWS . '/webchat/TEMPL/RICHIESTA_AMICIZIA/richiesta.php';

                // Inizializza una sessione cURL
                $ch = curl_init($url);

                // Imposta le opzioni di cURL
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($responseData));
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen(json_encode($responseData))
                ));

                // Ottieni i cookies della sessione
                $sessionCookies = session_id();

                // Imposta l'opzione CURLOPT_COOKIE per includere i cookies nella richiesta cURL
                curl_setopt($ch, CURLOPT_COOKIE, "PHPSESSID=" . $sessionCookies);


                // Esegui la richiesta POST
                $postResponse = curl_exec($ch);

                // Controlla se ci sono errori nella richiesta
                if (curl_errno($ch)) 
                {
                    echo 'Errore cURL: ' . curl_error($ch);
                } 
                else 
                {
                    // Stampa la risposta dal server di destinazione
                    echo $postResponse;
                }

                // Chiudi la sessione cURL
                curl_close($ch);

            } 
            else 
            {
                // Gestisci il caso in cui la risposta non sia un JSON valido
                echo 'Errore: risposta non valida dal server.';
            }
        }

        // Chiudi la sessione cURL
        curl_close($ch);

        
    }

    function accettaRichiesta($pathWS)
    {
        // Istanza della classe richiesteAmiciziaDMO
        $richieste = new clsRichiesteAmiciziaDMO;

        // Set dei valori inseriti
        $richieste->setNickMittente($_POST['mittente']);
        $richieste->setNickDestinatario($_POST['destinatario']);
        $richieste->setOrarioAccettata(date('Y-m-d H:i:s'));


        // URL del web service
        $url = 'http://' . $pathWS . '/webchat/WS/utenti/update.php';

        // Dati da inviare al web service in formato JSON
        $postData = json_encode(array(
            'azione' => 'accettaRichiesta',
            'mittente' => $richieste->getNickMittente(),
            'destinatario' => $richieste->getNickDestinatario(),
            'orarioAccettata' => $richieste->getOrarioAccettata()
        ));


        // Inizializza una sessione cURL
        $ch = curl_init($url);


        // Imposta le opzioni della richiesta cURL
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($postData)
        ));

        // Ottieni i cookies della sessione
        $sessionCookies = session_id();

        // Imposta l'opzione CURLOPT_COOKIE per includere i cookies nella richiesta cURL
        curl_setopt($ch, CURLOPT_COOKIE, "PHPSESSID=" . $sessionCookies);


        // Esegui la richiesta e ottieni la risposta
        $response = curl_exec($ch);
        if ($response === false) 
        {
            // Gestisci il caso in cui non ci sia stata nessuna risposta
            echo 'Nessuna risposta dal server.';
        } 
        else 
        {
            // Debug della risposta ricevuta
            echo 'Risposta dal server: ' . $response;

            // Decodifica la risposta JSON
            $responseData = json_decode($response, true);
            //var_dump($responseData);

            // Gestisci la risposta del web service
            if ($responseData != "" || $responseData != NULL)
            {
                if ($responseData['success'] == true || $responseData['success'] == false)
                {
                    session_start();

                    $_SESSION['esitoAccettazione'] = $responseData['message'];

                    if ($responseData['success'] == true) $_SESSION['ok'] = "true";
                    elseif ($responseData['success'] == false) $_SESSION['ok'] = "false";
                    
                    session_write_close();

                    header("Location: /webchat/PHP/CHAT/chat.php");
                }

            } 
            else 
            {
                // Gestisci il caso in cui la risposta non sia un JSON valido
                echo 'Errore: risposta non valida dal server.';
            }
        }

        // Chiudi la sessione cURL
        curl_close($ch);
    }

    function rifiutaRichiesta($pathWS)
    {

    }

?>
