<?php

    //importo la classe per accedere alle variabili
    require_once '../../DMO/richieste_amicizia.inc.php';

    //avvio la sessione per accederne alle variabili
    session_start();

    if ($_SERVER["REQUEST_METHOD"] == "POST") 
    {
        // Verifica quale pulsante è stato premuto
        if (isset($_POST['azione'])) 
        {

            if ($_POST['azione'] == 'INVIA') //invia richiesta amicizia
            {

                //istanzio la classe delle variabili delle richieste di amicizia
                $richiesteAmic = new clsRichiesteAmiciziaDB();

                //salvo tutte le variabili
                $richiesteAmic->setOrarioInvio(date("Y-m-d H:i:s"));
                $richiesteAmic->setNickMittente($_SESSION['nickUtente']);
                $richiesteAmic->setNickDestinatario($_POST['nickDestinatario']);

                $mitt = $richiesteAmic->getNickMittente();
                $dest = $richiesteAmic->getNickDestinatario();

                var_dump($mitt);
                var_dump($dest);

                //verifico che l'utente inserito non sia il proprio
                if ($mitt == $dest){
                    echo "qui";
                    header("Location: /Webchat/PHP/RICHIESTA_AMICIZIA/INVIA_RICHIESTA/inviaRichiesta.php?error=Non puoi inviare una richiesta di amicizia a te stesso!");
                }
                else
                {
                    echo "qua";

                    //devo verificare che l'utente inserito esista
                    $res = clsRichiesteAmiciziaBL::verificaEsistenzaUtente($richiesteAmic->getNickDestinatario());

                    
                    if ($res > 0) //utente esistente
                    {
                        //verifico se ho già l'utente loggato abbia già inviato una richiesta di amicizia al destinatario
                        $res = clsRichiesteAmiciziaBL::verificaEsistenzaRichiesta($richiesteAmic->getNickDestinatario(), $richiesteAmic->getNickMittente());

                        
                        if ($res == 1) header("Location: /Webchat/PHP/RICHIESTA_AMICIZIA/INVIA_RICHIESTA/inviaRichiesta.php?error=Hai gia inviato una richiesta di amicizia"); 
                        else
                        {
                            //richiamo il metodo per inviare la richiesta
                            clsRichiesteAmiciziaBL::inviaRichiesta($richiesteAmic->getOrarioInvio(), $richiesteAmic->getNickMittente(), $richiesteAmic->getNickDestinatario());
                        } 
                        
                    }
                    else
                        header("Location: /Webchat/PHP/RICHIESTA_AMICIZIA/INVIA_RICHIESTA/inviaRichiesta.php?error=Utente non esistente!");
                    
                    
                }

            }

        }
        elseif(isset($_POST['accetta']))    //accetta richiesta di amicizia
        {
            echo "ciao";
            
            
            //istanzio la classe delle variabili delle richieste di amicizia
            $richiesteAmic = new clsRichiesteAmiciziaDB();

            //salvo l'orario di accettazione della richiesta
            $richiesteAmic->setOrarioAccettata(date("Y-m-d H:i:s"));
            $richiesteAmic->setNickMittente($_POST['mittente']);
            $richiesteAmic->setNickDestinatario($_SESSION['nickUtente']);

            //echo $richiesteAmic->getOrarioAccettata();
            //echo $richiesteAmic->getNickMittente();
            //echo $richiesteAmic->getNickDestinatario();

            
            
            
            //richiamo il metodo per effettuare l'update
            clsRichiesteAmiciziaBL::accettaRichiesta($richiesteAmic->getNickMittente(), $richiesteAmic->getNickDestinatario(), $richiesteAmic->getOrarioAccettata());
            

        }
        elseif(isset($_POST['rifiuta']))    //rifiuta richiesta di amicizia
        {
            //istanzio la classe delle variabili delle richieste di amicizia
            $richiesteAmic = new clsRichiesteAmiciziaDB();
        }
    }


?>