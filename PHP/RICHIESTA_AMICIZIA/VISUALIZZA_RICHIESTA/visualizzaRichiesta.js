function aggiungiRichiesta()
{

    // Effettua una richiesta AJAX per ottenere il contenuto della pagina PHP
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var nuovoDiv = document.createElement('div');
            nuovoDiv.innerHTML = xhr.responseText;
            document.getElementById('richieste').appendChild(nuovoDiv);
        }
    };
    xhr.open('GET', '/webchat/TEMPL/RICHIESTA_AMICIZIA/richiesta.php', true);
    xhr.send();

}