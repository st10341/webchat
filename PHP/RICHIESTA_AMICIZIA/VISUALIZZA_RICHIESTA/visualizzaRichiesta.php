<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

        <!-- Latest compiled and minified CSS  -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- Latest compiled JavaScript -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>

        <link rel="stylesheet" href="visualizzaRichiesta.css">

        <script src="visualizzaRichiesta.js"></script>

</head>
<body onload="aggiungiRichiesta()">

    <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/webchat/TEMPL/HEADER/header.php'; ?> 

    <div class="rounded-3 p-3" id="richieste"> </div>

    <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/webchat/TEMPL/FOOTER/footer.php'; ?>
    
</body>
</html>