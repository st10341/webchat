<?


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>INVIA RICHIESTA</title>

    <!-- Latest compiled and minified CSS  -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Latest compiled JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>

    <link rel="stylesheet" href="inviaRichiesta.css">

</head>
<body>
    
    <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/webchat/TEMPL/HEADER/header.php'; ?> 

    <div class="rounded-3 p-3 centrato">

        <h2 style="text-align: center; margin: 10px; margin-top: 0;">Invia una richiesta di amicizia</h2>
        <hr style="margin-top: 0;">

        <form method="POST" action="/webchat/PHP/BL/UTENTE/utentePostBack.php">

            <input class="input" type="text" name="nickDestinatario" placeholder="Nickname" required>
            <input class="rounded-3 scelta" name="azione" type="submit" value="INVIA">

        </form>
            

    </div>



    <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/webchat/TEMPL/FOOTER/footer.php'; ?> 

</body>
</html>