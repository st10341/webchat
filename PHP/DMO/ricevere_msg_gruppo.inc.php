<?php
    class ricevereMsgGruppo
    {
        private $_orarioLettura;
        private $_destinatarioCF;
        private $_chattareGruppoID;


        // Funzione "get" per ottenere il valore di $_orarioLettura
        public function getOrarioLettura() {
            return $this->_orarioLettura;
        }

        // Funzione "set" per impostare il valore di $_orarioLettura
        public function setOrarioLettura($orarioLettura) {
            $this->_orarioLettura = $orarioLettura;
        }

        // Funzione "get" per ottenere il valore di $_destinatarioCF
        public function getDestinatarioCF() {
            return $this->_destinatarioCF;
        }

        // Funzione "set" per impostare il valore di $_destinatarioCF
        public function setDestinatarioCF($destinatarioCF) {
            $this->_destinatarioCF = $destinatarioCF;
        }

        // Funzione "get" per ottenere il valore di $_chattareGruppoID
        public function getChattareGruppoID() {
            return $this->_chattareGruppoID;
        }

        // Funzione "set" per impostare il valore di $_chattareGruppoID
        public function setChattareGruppoID($chattareGruppoID) {
            $this->_chattareGruppoID = $chattareGruppoID;
        }
    }

?>