<?
    class chattareGruppo
    {
        private $_ID;
        private $_messaggio;
        private $_orarioInvio;
        private $_stato;
        private $_utenteCF;
        private $_gruppoID;


        // Funzione "get" per ottenere il valore di $_ID
        public function getID() {
        return $this->_ID;
        }

        // Funzione "set" per impostare il valore di $_ID
        public function setID($ID) {
            $this->_ID = $ID;
        }

        // Funzione "get" per ottenere il valore di $_messaggio
        public function getMessaggio() {
            return $this->_messaggio;
        }

        // Funzione "set" per impostare il valore di $_messaggio
        public function setMessaggio($messaggio) {
            $this->_messaggio = $messaggio;
        }

        // Funzione "get" per ottenere il valore di $_orarioInvio
        public function getOrarioInvio() {
            return $this->_orarioInvio;
        }

        // Funzione "set" per impostare il valore di $_orarioInvio
        public function setOrarioInvio($orarioInvio) {
            $this->_orarioInvio = $orarioInvio;
        }

        // Funzione "get" per ottenere il valore di $_stato
        public function getStato() {
            return $this->_stato;
        }

        // Funzione "set" per impostare il valore di $_stato
        public function setStato($stato) {
            $this->_stato = $stato;
        }

        // Funzione "get" per ottenere il valore di $_utenteCF
        public function getUtenteCF() {
            return $this->_utenteCF;
        }

        // Funzione "set" per impostare il valore di $_utenteCF
        public function setUtenteCF($utenteCF) {
            $this->_utenteCF = $utenteCF;
        }

        // Funzione "get" per ottenere il valore di $_gruppoID
        public function getGruppoID() {
            return $this->_gruppoID;
        }

        // Funzione "set" per impostare il valore di $_gruppoID
        public function setGruppoID($gruppoID) {
            $this->_gruppoID = $gruppoID;
        }
    }

?>