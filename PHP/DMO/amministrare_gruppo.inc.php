<?
    class amministrareGruppo
    {
        private $_dataInizio;
        private $_dataFine;
        private $_utenteCF;
        private $_gruppoID;


        
        // Funzione "get" per ottenere il valore di $_dataInizio
        public function getDataInizio() {
            return $this->_dataInizio;
        }

        // Funzione "set" per impostare il valore di $_dataInizio
        public function setDataInizio($dataInizio) {
            $this->_dataInizio = $dataInizio;
        }

        // Funzione "get" per ottenere il valore di $_dataFine
        public function getDataFine() {
            return $this->_dataFine;
        }

        // Funzione "set" per impostare il valore di $_dataFine
        public function setDataFine($dataFine) {
            $this->_dataFine = $dataFine;
        }

        // Funzione "get" per ottenere il valore di $_utenteCF
        public function getUtenteCF() {
            return $this->_utenteCF;
        }

        // Funzione "set" per impostare il valore di $_utenteCF
        public function setUtenteCF($utenteCF) {
            $this->_utenteCF = $utenteCF;
        }

        // Funzione "get" per ottenere il valore di $_gruppoID
        public function getGruppoID() {
            return $this->_gruppoID;
        }

        // Funzione "set" per impostare il valore di $_gruppoID
        public function setGruppoID($gruppoID) {
            $this->_gruppoID = $gruppoID;
        }
    }

?>