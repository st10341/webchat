<?php
    
    class clsChattareUtenteDMO
    {
        private $_ID;
        private $_messaggio;
        private $_orarioInvio;
        private $_orarioLettura;
        private $_stato;
        private $_mittente;
        private $_destinatario;


        // Funzione "get" per ottenere il valore di $_ID
        public function getID() {
        return $this->_ID;
        }

        // Funzione "set" per impostare il valore di $_ID
        public function setID($ID) {
            $this->_ID = $ID;
        }

        // Funzione "get" per ottenere il valore di $_messaggio
        public function getMessaggio() {
            return $this->_messaggio;
        }

        // Funzione "set" per impostare il valore di $_messaggio
        public function setMessaggio($messaggio) {
            $this->_messaggio = $messaggio;
        }

        // Funzione "get" per ottenere il valore di $_orarioInvio
        public function getOrarioInvio() {
            return $this->_orarioInvio;
        }

        // Funzione "set" per impostare il valore di $_orarioInvio
        public function setOrarioInvio($orarioInvio) {
            $this->_orarioInvio = $orarioInvio;
        }

        // Funzione "get" per ottenere il valore di $_orarioLettura
        public function getOrarioLettura() {
            return $this->_orarioLettura;
        }

        // Funzione "set" per impostare il valore di $_orarioLettura
        public function setOrarioLettura($orarioLettura) {
            $this->_orarioLettura = $orarioLettura;
        }

        // Funzione "get" per ottenere il valore di $_stato
        public function getStato() {
            return $this->_stato;
        }

        // Funzione "set" per impostare il valore di $_stato
        public function setStato($stato) {
            $this->_stato = $stato;
        }

        // Funzione "get" per ottenere il valore di $_mittenteCF
        public function getMittente() {
            return $this->_mittente;
        }

        // Funzione "set" per impostare il valore di $_mittenteCF
        public function setMittente($mittente) {
            $this->_mittente = $mittente;
        }

        // Funzione "get" per ottenere il valore di $_destinatarioCF
        public function getDestinatario() {
            return $this->_destinatario;
        }

        // Funzione "set" per impostare il valore di $_destinatarioCF
        public function setDestinatario($destinatario) {
            $this->_destinatario = $destinatario;
        }
    }

?>