<?php

    class clsRichiesteAmiciziaDMO
    {
        private $_ID;
        private $_orarioInvio;
        private $_orarioAccettata;
        private $_nickMittente;
        private $_nickDestinatario;


        // Costruttore
        public function __construct()
        {
            
        }

        // Funzione set per _ID
        public function setID($id){
            $this->_ID = $id;
        }

        // Funzione get per _ID
        public function getID(){
            return $this->_ID;
        }

        // Funzione set per _orarioInvio
        public function setOrarioInvio($orarioInvio) {
            $this->_orarioInvio = $orarioInvio;
        }

        // Funzione get per _orarioInvio
        public function getOrarioInvio() {
            return $this->_orarioInvio;
        }

        // Funzione set per _orarioAccettata
        public function setOrarioAccettata($orarioAccettata) {
            $this->_orarioAccettata = $orarioAccettata;
        }

        // Funzione get per _orarioAccettata
        public function getOrarioAccettata() {
            return $this->_orarioAccettata;
        }

        // Funzione set per _nickMittente
        public function setNickMittente($nickMittente) {
            $this->_nickMittente = $nickMittente;
        }

        // Funzione get per _nickMittente
        public function getNickMittente() {
            return $this->_nickMittente;
        }

        // Funzione set per _nickDestinatario
        public function setNickDestinatario($nickDestinatario) {
            $this->_nickDestinatario = $nickDestinatario;
        }

        // Funzione get per _nickDestinatario
        public function getNickDestinatario() {
            return $this->_nickDestinatario;
        }
        
    }

?>