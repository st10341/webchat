<?
    class partecipareGruppo
    {
        private $_dataEntrata;
        private $_dataUscita;
        private $_utenteCF;
        private $_gruppoID;


        // Funzione "get" per ottenere il valore di $_dataEntrata
        public function getDataEntrata() {
            return $this->_dataEntrata;
        }

        // Funzione "set" per impostare il valore di $_dataEntrata
        public function setDataEntrata($dataEntrata) {
            $this->_dataEntrata = $dataEntrata;
        }

        // Funzione "get" per ottenere il valore di $_dataUscita
        public function getDataUscita() {
            return $this->_dataUscita;
        }

        // Funzione "set" per impostare il valore di $_dataUscita
        public function setDataUscita($dataUscita) {
            $this->_dataUscita = $dataUscita;
        }

        // Funzione "get" per ottenere il valore di $_utenteCF
        public function getUtenteCF() {
            return $this->_utenteCF;
        }

        // Funzione "set" per impostare il valore di $_utenteCF
        public function setUtenteCF($utenteCF) {
            $this->_utenteCF = $utenteCF;
        }

        // Funzione "get" per ottenere il valore di $_gruppoID
        public function getGruppoID() {
            return $this->_gruppoID;
        }

        // Funzione "set" per impostare il valore di $_gruppoID
        public function setGruppoID($gruppoID) {
            $this->_gruppoID = $gruppoID;
        }
    }

?>