<?php 
    class clsUtenteDMO
    {
        private $_nome;
        private $_cognome;
        private $_dataNascita;
        private $_userName;
        private $_password;
        private $_email;

        // Costruttore
        public function __construct()
        {
            
        }

        #region NOME
        public function getNome(){
            return $this -> _nome;
        }

        public function setNome($nome){
            if(!empty($nome)) $this->_nome = $nome;
        }
        #endregion


        #region COGNOME
        public function getCognome(){
            return $this -> _cognome;
        }

        public function setCognome($cognome){
            if(!empty($cognome)) $this->_cognome = $cognome;
        }
        #endregion


        #region DATA NASCITA
        public function getDataNascita(){
            return $this -> _dataNascita;
        }

        public function setDataNascita($dataNascita){
            if(!empty($dataNascita)) $this->_dataNascita = $dataNascita;
        }
        #endregion


        #region USER NAME
        public function getUserName(){
            return $this -> _userName;
        }

        public function setUserName($userName){
            if(!empty($userName)) $this->_userName = $userName;
        }
        #endregion


        #region PASSWORD
        public function getPassword(){
            return $this -> _password;
        }

        public function setPassword($password){
            if(!empty($password)) $this->_password = $password;
        }
        #endregion


        #region EMAIL
        public function getEmail(){
            return $this -> _email;
        }

        public function setEmail($email){
            if(!empty($email)) $this->_email = $email;
        }
        #endregion
    }
?>

