<?
    class gruppo
    {
        private $_ID;
        private $_nome;
        private $_descrizione;
        private $_dataCreazione;
        private $_argomento;


        // Funzione "get" per ottenere il valore di $_ID
        public function getID() {
        return $this->_ID;
        }

        // Funzione "set" per impostare il valore di $_ID
        public function setID($ID) {
            $this->_ID = $ID;
        }

        // Funzione "get" per ottenere il valore di $_nome
        public function getNome() {
            return $this->_nome;
        }

        // Funzione "set" per impostare il valore di $_nome
        public function setNome($nome) {
            $this->_nome = $nome;
        }

        // Funzione "get" per ottenere il valore di $_descrizione
        public function getDescrizione() {
            return $this->_descrizione;
        }

        // Funzione "set" per impostare il valore di $_descrizione
        public function setDescrizione($descrizione) {
            $this->_descrizione = $descrizione;
        }

        // Funzione "get" per ottenere il valore di $_dataCreazione
        public function getDataCreazione() {
            return $this->_dataCreazione;
        }

        // Funzione "set" per impostare il valore di $_dataCreazione
        public function setDataCreazione($dataCreazione) {
            $this->_dataCreazione = $dataCreazione;
        }

        // Funzione "get" per ottenere il valore di $_argomento
        public function getArgomento() {
            return $this->_argomento;
        }

        // Funzione "set" per impostare il valore di $_argomento
        public function setArgomento($argomento) {
            $this->_argomento = $argomento;
        }
    }

?>