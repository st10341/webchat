<?php

    if ($_SERVER['REQUEST_METHOD'] === 'POST')
    {
        //avvio la sessione per poter accedere alle variabili
        session_start();

        // Recupera il codice OTP dalla sessione
        if (isset($_SESSION['codiceOTP'])) $otpCode = $_SESSION['codiceOTP'];
        else $otpCode = 'Valore non disponibile';
        

        //recupero il codice inserito dall'utente
        $otpUser1 = $_POST['code1'];
        $otpUser2 = $_POST['code2'];
        $otpUser3 = $_POST['code3'];
        $otpUser4 = $_POST['code4'];
        $otpUser5 = $_POST['code5'];
        $otpUser6 = $_POST['code6'];

        $otpCompleto = $otpUser1 . $otpUser2 . $otpUser3 . $otpUser4 . $otpUser5 . $otpUser6;


        if ($otpCode === $otpCompleto) header("Location: resetPsw.php");
        else echo "codice errato";
    }
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VERIFICA CODICE</title>

    <!-- Latest compiled and minified CSS  -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Latest compiled JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>

    <link rel="stylesheet" href="../../CSS/style.css">


    <script>
        // Limita l'input a un solo numero
        function limitaUnSoloNumero(input) 
        {
            input.value = input.value.replace(/\D/g, '').substring(0, 1);
        }

        let tempoRimanente = 60;
        // Funzione per aggiornare il countdown
        function aggiornaCountdown() {
            const countdownParagrafo = document.getElementById('countdown');
            
            // Se il countdown è ancora attivo
            if (tempoRimanente >= 0) {
                countdownParagrafo.textContent = `Puoi richiedere un nuovo codice tra ${tempoRimanente} secondi`;
                tempoRimanente--;
                setTimeout(aggiornaCountdown, 1000);
            } else {
                // Quando il countdown raggiunge 0
                countdownParagrafo.textContent = 'Richiedi nuovo OTP';
                countdownParagrafo.style.cursor = 'pointer';

                // Aggiungi un gestore di eventi clic al paragrafo
                countdownParagrafo.addEventListener('click', function() {
                    // Reindirizza a una pagina PHP
                    window.location.href = 'invioOTP.php';
                });
            }
        }

        window.onload = function() {
            aggiornaCountdown();
        };
    </script>

</head>
<body>

    <div id="divisoreLogin" class="rounded-4">
        <h3 style="text-align: center;">Inserisci OTP</h3>
        <p style="text-align: center;">Abbiamo inviato un codice alla tua email</p>
        <hr>
        

        <form method="POST" action="verificaOTP.php">
            <div class="row">

                <div class="col-2">
                    <input type="text" oninput="limitaUnSoloNumero(this)" class="otpBlocks" name="code1" id="code1" required>
                </div>


                <div class="col-2">
                    <input type="text" oninput="limitaUnSoloNumero(this)" class="otpBlocks" name="code2" id="code2" required>
                </div>


                <div class="col-2">
                    <input type="text" oninput="limitaUnSoloNumero(this)" class="otpBlocks" name="code3" id="code3" required>
                </div>


                <div class="col-2">
                <input type="text" oninput="limitaUnSoloNumero(this)" class="otpBlocks" name="code4" id="code4" required>
                </div>


                <div class="col-2">
                    <input type="text" oninput="limitaUnSoloNumero(this)" class="otpBlocks" name="code5" id="code5" required>
                </div>


                <div class="col-2">
                <input type="text" oninput="limitaUnSoloNumero(this)" class="otpBlocks" name="code6" id="code6" required>
                </div>

            </div>

            <hr>

            <p id="countdown" style="text-align: center;"></p>

            <div class="col">
                <input type="submit" class="scelta rounded-3" value="INVIA">
            </div>

        </form>
        

    </div>
</body>
</html>