<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RESET PASSWORD</title>

    <!-- Latest compiled and minified CSS  -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Latest compiled JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>

    <link rel="stylesheet" href="../../CSS/style.css">

    <script>

        function controllaTextBox()
        {
            var textBoxPsw = document.getElementById('psw');
            var textBoxConfermaPsw = document.getElementById('ConfermaPsw');

            var psw = textBoxPsw.value; // Correzione del nome della variabile
            var confermaPsw = textBoxConfermaPsw.value;

            if (psw === confermaPsw)
            {
                var psw = textBoxPw.value;
                var contieneMaiuscola = /[A-Z]/.test(psw);
                var contieneMinuscola = /[a-z]/.test(psw);
                var carattereSpeciale = /[!@#$%^&*(),.?":{}|<>]/.test(psw);

                if (psw.length >= 8 && contieneMaiuscola && contieneMinuscola && carattereSpeciale) {
                    return true;
                } else {
                    alert("La password non soddisfa i requisiti.");
                    return false;
                }
            }
            else alert("Le due password non sono uguali!");
        }


        document.addEventListener("DOMContentLoaded", function () 
        {
            var imgLogPswEye = document.getElementById("RegPswEye");
            var passwordInputLog = document.getElementById("psw");

            imgLogPswEye.addEventListener("click", function () {
                var hideImgURL = "/webchat/IMG/hide.png";
                var showImgURL = "/webchat/IMG/show.png";

                if (passwordInputLog.type === "password") {
                    passwordInputLog.type = "text";
                    imgLogPswEye.src = hideImgURL;
                } else {
                    passwordInputLog.type = "password";
                    imgLogPswEye.src = showImgURL;
                }
            });

            var imgRegPswEye = document.getElementById("LogPswEye");
            var passwordInputReg = document.getElementById("ConfermaPsw");

            imgRegPswEye.addEventListener("click", function () {
                var hideImgURL = "/webchat/IMG/hide.png";
                var showImgURL = "/webchat/IMG/show.png";

                if (passwordInputReg.type === "password") {
                    passwordInputReg.type = "text";
                    imgRegPswEye.src = hideImgURL;
                } else {
                    passwordInputReg.type = "password";
                    imgRegPswEye.src = showImgURL;
                }
            });
        });
        
    </script>

</head>
<body>

    <form id="divisoreLogin" class="rounded-4" onsubmit="return controllaTextBox()" method="POST" action="/webchat/PHP/BL/UTENTE/utentePostBack.php">

    <h2 style="text-align: center;">
        Reimposta password
    </h2>

    <hr>

    <div class="container">
        <div class="row">

            <div class="col-10 p-0" style="padding-right: 0;">
                <input class="input " type="password" name="psw" id="psw" placeholder="Nuova password" required style="border-right: 0; border-top-left-radius: 8px; border-bottom-left-radius: 8px;">
                
                <img src="/webchat/IMG/show.png" id="RegPswEye" style="z-index: 0;">

            </div>

            <div class="col-2" style="padding: 0;">
                <input class="input" id="barbatrucco" style="border-top-right-radius: 8px; border-bottom-right-radius: 8px; border-left: 0;">
            </div>

        </div>
    </div>


    <div class="container">
        <div class="row">

            <div class="col-10 p-0" style="padding-right: 0;">
                <input class="input " type="password" name="ConfermaPsw" id="ConfermaPsw" placeholder="Conferma password" required onkeydown="bloccaTab(event)" style="border-right: 0; border-top-left-radius: 8px; border-bottom-left-radius: 8px;">
                
                <img src="/webchat/IMG/show.png" id="LogPswEye" style="z-index: 0;">

            </div>

            <div class="col-2" style="padding: 0;">
                <input class="input" id="barbatrucco" style="border-top-right-radius: 8px; border-bottom-right-radius: 8px; border-left: 0;">
            </div>

        </div>
    </div>



    <hr class="mt-0">


    <strong>La password è accettata se soddisfa i seguenti requisiti:</strong>
    <ul id="passwordCriteria">
        <li id="lengthCriteria" style="color: red;">Almeno 8 caratteri</li>
        <li id="uppercaseCriteria" style="color: red;">Almeno una lettera maiuscola</li>
        <li id="lowercaseCriteria" style="color: red;">Almeno una lettera minuscola</li>
        <li id="specialCharacterCriteria" style="color: red;">Almeno un carattere speciale</li>
        <li id="equal" style="color: red;">Password uguali</li>
    </ul>

    <div class="col">
       <input class="scelta rounded-3" id="btRecupera" type="submit" name="azione" value="AVANTI">
    </div>
    

    </form>


    <script>
        var passwordInput = document.getElementById('psw');
        var passwordSecond = document.getElementById('ConfermaPsw');
        var lengthCriteria = document.getElementById('lengthCriteria');
        var uppercaseCriteria = document.getElementById('uppercaseCriteria');
        var lowercaseCriteria = document.getElementById('lowercaseCriteria');
        var specialCharacterCriteria = document.getElementById('specialCharacterCriteria');
        var equalCriteria = document.getElementById('equal');

        passwordInput.addEventListener('input', function() {
            var password = this.value;

            // Controllo lunghezza password
            if (password.length >= 8) {
                lengthCriteria.style.color = 'green';
            } else {
                lengthCriteria.style.color = 'red';
            }

            // Controllo presenza di lettera maiuscola
            if (/[A-Z]/.test(password)) {
                uppercaseCriteria.style.color = 'green';
            } else {
                uppercaseCriteria.style.color = 'red';
            }

            // Controllo presenza di lettera minuscola
            if (/[a-z]/.test(password)) {
                lowercaseCriteria.style.color = 'green';
            } else {
                lowercaseCriteria.style.color = 'red';
            }

            // Controllo presenza di carattere speciale
            if (/[!@#$%^&*(),.?":{}|<>]/.test(password)) {
                specialCharacterCriteria.style.color = 'green';
            } else {
                specialCharacterCriteria.style.color = 'red';
            }

            // Controllo uguaglianza password
            var primaPSW = passwordInput.value;
            var secondaPSW = passwordSecond.value;

            if (primaPSW === secondaPSW) {
                equalCriteria.style.color = 'green';
            } else {
                equalCriteria.style.color = 'red';
            }
        });

        passwordSecond.addEventListener('input', function() {
            // Controllo uguaglianza password
            var primaPSW = passwordInput.value;
            var secondaPSW = passwordSecond.value;

            if (primaPSW === secondaPSW) {
                equalCriteria.style.color = 'green';
            } else {
                equalCriteria.style.color = 'red';
            }
        });
    </script>

    <script>
        function bloccaTab(event) {
            if (event.key === 'Tab') {
                event.preventDefault();
            }
        }
    </script>
    
</body>
</html>