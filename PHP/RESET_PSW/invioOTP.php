<?php
    
    //accedo alle variabili in sessione
    session_start();


    function generateOTP($length = 6) {
        // Caratteri consentiti nella generazione dell'OTP
        $characters = '0123456789';

        $otp = '';
        $charactersLength = strlen($characters);

        // Genera il codice OTP casuale
        for ($i = 0; $i < $length; $i++) {
            $otp .= $characters[rand(0, $charactersLength - 1)];
        }

        return $otp;
    }
    

    //genero il codice otp
    $otpCode = generateOTP();

    //mi salvo il codice in sessione per fare il controllo successivo
    $_SESSION['codiceOTP'] = $otpCode;


    // Recupera il valore dalla sessione
    if (isset($_SESSION['email'])) 
    {
        $email = $_SESSION['email'];
    } 
    else 
    {
        $email = 'Valore non disponibile';
    }

    $to = $email;
    $subject = "Il tuo codice OTP - non condividerlo con nessuno";
    $message = "Utilizza il seguente codice per il recupero della password: $otpCode";
    
    $headers = "From: mittente@example.com\r\n";
    $headers .= "Reply-To: mittente@example.com\r\n";
    $headers .= "Content-Type: text/html;charset=utf-8\r\n";

    
    // Invia l'email
    if (mail($to, $subject, $message, $headers)) 
    {
        echo "Email inviata con successo a $email - $otpCode";
        header("Location: verificaOTP.php");
    } else {
        echo "Errore durante l'invio dell'email a $email";
    }


    

?>