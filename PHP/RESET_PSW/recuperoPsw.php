<!DOCTYPE html>
<html lang="IT">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RECUPERA PASSWORD</title>

    <!-- Latest compiled and minified CSS  -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Latest compiled JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>

    <link rel="stylesheet" href="../../CSS/style.css">

</head>
<body>

    <form id="divisoreLogin" class="rounded-3" method="POST" action="/webchat/PHP/BL/UTENTE/utentePostBack.php">

        <h2 style="text-align: center;">
            Inserire l'e-mail
        </h2>

        <hr>

        <div>
            <input style="margin-bottom: 16px;" class="input rounded-3" type="email" name="email" id="email" placeholder="Email" required><br>
        </div>

        <hr class="mt-0">

        <div class="row">
            <div class="col">
                <input class="scelta rounded-3" id="btRecupera" type="submit" name="azione" value="RECUPERA">
            </div>
        </div>

        

    </form>
    
</body>
</html>