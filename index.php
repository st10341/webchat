<?php
    session_start();

    if(isset($_SESSION['amicoCliccato'])) unset($_SESSION['amicoCliccato']);

    if (isset($_SESSION['loginErr']))
    {
        $errore = $_SESSION['loginErr'];
        unset($_SESSION['loginErr']);
    }
    
    //set su una variabile il percorso
    $pathIndex = dirname(__FILE__);
    //path da usare sui require
    $_SESSION['pathRequire'] = $pathIndex;

    $parts = explode('/', $pathIndex);

    // Find the part that contains the domain
    $domain = '';
    foreach ($parts as $part) 
    {
        if (strpos($part, '.') !== false) {
            $domain = $part;
            break;
        }
        else $domain = false;
    }
    
    if ($domain == false) $_SESSION['pathWS'] = '/localhost';
    else $_SESSION['pathWS'] = $domain;

    session_write_close();
?>

<!DOCTYPE html>
<html lang="it">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>ACCESSO</title>

        <?php
        
            // // Include della classe utenteBL
            include_once $pathIndex.'/PHP/BL/UTENTE/utente_metodi.php';


            if(isset($_COOKIE['utenteLoggato']))
            {
                //acquisisco il nickname contenuto nel cookie
                $utente = $_COOKIE['utenteLoggato'];

            
                //controllo per motivi di sicurezza se l'utente esiste
                $res = clsUtenteBL::controllaNick($utente);
                
                session_start();
                // Insert nickname in sessione
                $_SESSION['nickUtente'] = $utente;
                session_write_close();

                //utente esistente nel db, quindi redirect alla chat
                if ($res) header("Location: /webchat/PHP/CHAT/chat.php");
            }


        ?>

        <!-- Latest compiled and minified CSS  -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- Latest compiled JavaScript -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>

        <link rel="stylesheet" href="CSS/style.css">
        <link rel="stylesheet" href="PHP/global.css">


        <script>
            function controllaTextBox() 
            {
                var textBoxPw = document.getElementById('password');
                var pw = textBoxPw.value;
                var contieneMaiuscola = /[A-Z]/.test(pw);
                var contieneMinuscola = /[a-z]/.test(pw);
                var carattereSpeciale = /[!@#$%^&*(),.?":{}|<>]/.test(pw);

                if (pw.length >= 8 && contieneMaiuscola && contieneMinuscola && carattereSpeciale) {
                    return true;
                } else {
                    alert("La password non soddisfa i requisiti.");
                    return false;
                }
            }

            function mostraCampi(tipoForm) 
            {
                const formLogin = document.getElementById('formLogin');
                const formRegistrati = document.getElementById('formRegister');

                var loginButton = document.getElementById('loginButton');
                var registerButton = document.getElementById('registerButton');

                if (tipoForm === 'login') {
                    formLogin.classList.remove('hidden');
                    formRegistrati.classList.add('hidden');
                    loginButton.classList.add('color');
                    registerButton.classList.remove('color');
                } else if (tipoForm === 'register') {
                    formLogin.classList.add('hidden');
                    formRegistrati.classList.remove('hidden');
                    registerButton.classList.add('color');
                    loginButton.classList.remove('color');
                }
            }


            document.addEventListener("DOMContentLoaded", function () 
            {
                var imgLogPswEye = document.getElementById("LogPswEye");
                var passwordInputLog = document.getElementById("LogPassword");

                imgLogPswEye.addEventListener("click", function () {
                    var hideImgURL = "/webchat/IMG/hide.png";
                    var showImgURL = "/webchat/IMG/show.png";

                    if (passwordInputLog.type === "password") {
                        passwordInputLog.type = "text";
                        imgLogPswEye.src = hideImgURL;
                    } else {
                        passwordInputLog.type = "password";
                        imgLogPswEye.src = showImgURL;
                    }
                });

                var imgRegPswEye = document.getElementById("RegPswEye");
                var passwordInputReg = document.getElementById("password");

                imgRegPswEye.addEventListener("click", function () {
                    var hideImgURL = "/webchat/IMG/hide.png";
                    var showImgURL = "/webchat/IMG/show.png";

                    if (passwordInputReg.type === "password") {
                        passwordInputReg.type = "text";
                        imgRegPswEye.src = hideImgURL;
                    } else {
                        passwordInputReg.type = "password";
                        imgRegPswEye.src = showImgURL;
                    }
                });
            });

        </script>

    </head>
    
    <body>

        <?php if(isset($errore)): ?>

            <div id="errore">
                <p><?php echo $errore; ?></p>
            </div>
        
        <?php endif; ?>

        


        <div id="divisoreLogin" class="rounded-4">

            <div>

                <div class="row">
                    <div class="col">
                        <input id="loginButton" class="scelta color rounded-3" type="submit" value="LOGIN" onclick="mostraCampi('login')">
                    </div>

                    <div class="col">
                        <input id="registerButton" class="scelta rounded-3" type="submit" value="SIGN-UP" onclick="mostraCampi('register')">
                    </div>
                </div>

            </div>


            <form id="formRegister" onsubmit="return controllaTextBox()" class="hidden" action="/webchat/PHP/BL/UTENTE/utentePostBack.php" method="POST">

                <h2 style="text-align: center; margin: 10px;">Register</h2>
                <hr style="margin-top: 0;">

                <!-- NOME COGNOME-->
                <div class="row">

                    <div class="col">
                        <input class="input rounded-3" type="text" name="RegNome" id="nome" value="" placeholder="Nome" required>
                    </div>

                    <div class="col">
                        <input class="input rounded-3" type="text" name="RegCognome" id="cognome" placeholder="Cognome" required>
                    </div>

                </div>

                <!-- DATA NASCITA USERNAME -->
                <div class="row">

                    <div class="col">
                        <input class="input rounded-3" type="date" name="RegDataNascita" id="dataNascita" required>
                    </div>

                    <div class="col">
                        <input class="input rounded-3" type="text" name="RegUserName" id="userName" placeholder="Username" required>
                    </div>

                </div>
                
                <script>
                    // Aggiungi un gestore per l'evento change all'elemento input
                    document.getElementById('dataNascita').addEventListener('change', function() 
                    {
                        // Ottieni la data di nascita inserita dall'utente
                        var birthDate = new Date(this.value);

                        // Ottieni la data di oggi
                        var today = new Date();

                        // Sottrai 14 anni dalla data di oggi
                        var minDate = new Date(today.getFullYear() - 14, today.getMonth(), today.getDate());

                        // Confronta la data di nascita inserita con la data minima consentita
                        if (birthDate > minDate) 
                        {
                            alert("Devi avere almeno 14 anni per registrarti!");
                            // Reimposta il campo data di nascita
                            this.value = '';
                        }
                    });
                </script>
                
                <!-- EMAIL -->
                <div class="container">
                    <div class="row">

                        <div class="col-12 p-0">
                            <input class="input rounded-3" type="email" name="RegEmail" id="email" placeholder="Email address" required>
                        </div>
                        
                    </div>
                </div>
                
                <!-- PASSWORD -->
                <div class="container">
                    <div class="row">

                        <div class="col-10 p-0" style="padding-right: 0;">
                            <input class="input " type="password" name="RegPassword" id="password" placeholder="Password" required style="border-right: 0; border-top-left-radius: 8px; border-bottom-left-radius: 8px;">
                            
                            <img src="/webchat/IMG/show.png" id="RegPswEye" style="z-index: 0;">

                        </div>

                        <div class="col-2" style="padding: 0;">
                            <input class="input" id="barbatrucco" style="border-top-right-radius: 8px; border-bottom-right-radius: 8px; border-left: 0;">
                        </div>

                    </div>
                </div>



                <hr>


                <strong>La password è accettata se soddisfa i seguenti requisiti:</strong>
                <ul id="passwordCriteria">
                    <li id="lengthCriteria" style="color: red;">Almeno 8 caratteri</li>
                    <li id="uppercaseCriteria" style="color: red;">Almeno una lettera maiuscola</li>
                    <li id="lowercaseCriteria" style="color: red;">Almeno una lettera minuscola</li>
                    <li id="specialCharacterCriteria" style="color: red;">Almeno un carattere speciale</li>
                </ul>

                <div class="row">

                    <div class="col">
                        <input id="SIGN-UP" class="scelta rounded-3" type="submit" name="azione" value="SIGN-UP">
                    </div>
                    
                </div>

            </form>

            <script>
                var passwordInput = document.getElementById('password');
                var lengthCriteria = document.getElementById('lengthCriteria');
                var uppercaseCriteria = document.getElementById('uppercaseCriteria');
                var lowercaseCriteria = document.getElementById('lowercaseCriteria');
                var specialCharacterCriteria = document.getElementById('specialCharacterCriteria');


                passwordInput.addEventListener('input', function() 
                {
                    var password = this.value;

                    // Controllo lunghezza password
                    if (password.length >= 8) {
                        lengthCriteria.style.color = 'green';
                    } else {
                        lengthCriteria.style.color = 'red';
                    }

                    // Controllo presenza di lettera maiuscola
                    if (/[A-Z]/.test(password)) {
                        uppercaseCriteria.style.color = 'green';
                    } else {
                        uppercaseCriteria.style.color = 'red';
                    }

                    // Controllo presenza di lettera minuscola
                    if (/[a-z]/.test(password)) {
                        lowercaseCriteria.style.color = 'green';
                    } else {
                        lowercaseCriteria.style.color = 'red';
                    }

                    // Controllo presenza di carattere speciale
                    if (/[!@#$%^&*(),.?":{}|<>]/.test(password)) {
                        specialCharacterCriteria.style.color = 'green';
                    } else {
                        specialCharacterCriteria.style.color = 'red';
                    }
                });
            </script>

            

            
            <form id="formLogin" action="/webchat/PHP/BL/UTENTE/utentePostBack.php" method="POST">

                <h2 style="text-align: center; margin: 10px;">Login</h2>
                <hr style="margin-top: 0;">

                <div class="row">

                    <div class="col">
                        <input class="input rounded-3" type="email" name="LogEmail" placeholder="Email" required><br>
                    </div>

                </div>

                <div class="row">

                    <div class="col-10" style="padding-right: 0;">

                        <input class="input " type="password" name="LogPassword" id="LogPassword" placeholder="Password" onkeydown="bloccaTab(event)"  style="border-right: 0; border-top-left-radius: 8px; border-bottom-left-radius: 8px;">
                        
                        <img src="/webchat/IMG/show.png" id="LogPswEye" style="z-index: 0;">
                        
                    </div>

                    <div class="col-2" style="padding-left: 0;">
                        <input class="input" id="barbatrucco" style="border-top-right-radius: 8px; border-bottom-right-radius: 8px; border-left: 0;">
                    </div>

                    
                    
                </div>  


                <p><a href="PHP/RESET_PSW/recuperoPsw.php">Password dimenticata?</a> </p>

                <hr>

                <div class="row">

                    <div class="col">
                        <input id="LOGIN" class="scelta rounded-3" type="submit" name="azione" value="LOGIN">
                    </div>
                    
                </div>
                
            </form>

            

            <!-- <script>

                var img = document.getElementById("LogPswEye");
                var passwordInput = document.getElementById("LogPassword");

                img.addEventListener("click", function() 
                {
                    
                    // Otteniamo l'URL delle immagini per la password nascosta e visibile
                    var hideImgURL = "/webchat/IMG/hide.png";
                    var showImgURL = "/webchat/IMG/show.png";

                    // Se la password è nascosta, mostriamola
                    if (passwordInput.type === "password") 
                    {
                        passwordInput.type = "text";
                        img.src = hideImgURL; // Cambia l'immagine dell'occhio per nascondere la password
                    } 
                    else 
                    {
                        // Altrimenti, nascondiamola
                        passwordInput.type = "password";
                        img.src = showImgURL; // Cambia l'immagine dell'occhio per mostrare la password
                    }
                });


                var img = document.getElementById("RegPswEye");
                var passwordInput = document.getElementById("password");

                img.addEventListener("click", function() 
                {
                    
                    // Otteniamo l'URL delle immagini per la password nascosta e visibile
                    var hideImgURL = "/webchat/IMG/hide.png";
                    var showImgURL = "/webchat/IMG/show.png";

                    // Se la password è nascosta, mostriamola
                    if (passwordInput.type === "password") 
                    {
                        passwordInput.type = "text";
                        img.src = hideImgURL; // Cambia l'immagine dell'occhio per nascondere la password
                    } 
                    else 
                    {
                        // Altrimenti, nascondiamola
                        passwordInput.type = "password";
                        img.src = showImgURL; // Cambia l'immagine dell'occhio per mostrare la password
                    }
                });

            </script> -->

            

        </div>
    
        <!--<script src="./SCRIPTS/index.js"></script>-->

        <script>
            function bloccaTab(event) {
                if (event.key === 'Tab') {
                    event.preventDefault();
                }
            }
        </script>


    </body>
</html>