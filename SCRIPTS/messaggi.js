// In questo file verranno gestiti tutti i messaggi scambiati tra l'utente in sessione e quello cliccato dall'elenco

//EVENTO CLICK BOTTONE INVIO MESSAGGIO
document.addEventListener("DOMContentLoaded", function() {
    var bottone = document.getElementById("inviaMsg");
          
    bottone.addEventListener("click", inviaMessaggio);
});


function getMessaggi()
{
    // Acquisisco i valori da inviare al metodo post
    var dati = 
    {
        azione: "getMessaggi",
        utente1: document.getElementById("amico").value,
        utente2: document.getElementById("utenteLoggato").value
    };

    // Stabilisco la modalità di comunicazione con il server
    var opzioni = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(dati)
    };

    // Esegui una richiesta al servizio Web per ottenere l'elenco degli utenti
    fetch('http://localhost/webchat/WS/UTENTI/read.php')

    .then(response => {
        if (!response.ok) {
            throw new Error('Network response was not ok ' + response.statusText);
        }
        return response.json(); // Supponendo che il server risponda con JSON
    })

    .then(data => {
        // Dati ottenuti dalla richiesta
        // Ora invia i dati alla pagina contatto.php tramite POST
        const formData = new FormData();
        formData.append('msg', JSON.stringify(data)); // Converti i dati in formato JSON e aggiungili al FormData

        // Invia la richiesta POST
        fetch('http://localhost/webchat/TEMPL/CONTATTO/messaggio.php', {
            method: 'POST',
            body: formData
        })

        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok ' + response.statusText);
            }
        })

        .catch(error => {
            console.error('There was a problem with the POST request:', error);
        });
    })
    .catch(error => {
        console.error('There was a problem with the fetch operation:', error);
    });
}

function inviaMessaggio()
{
    // Acquisisco i valori da inviare al metodo post
    var dati = 
    {
        azione: "inviaMsg",
        messaggio: document.getElementById("messaggio").value,
        orarioInvio: document.getElementById("dataInvio").value,
        stato: "S",
        mittente: document.getElementById("mittente").value,
        destinatario: document.getElementById("destinatario").value
    };

    // Stabilisco la modalità di comunicazione con il server
    var opzioni = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(dati)
    };

    fetch('http://localhost/webchat/WS/utenti/create.php', opzioni)

    .then(response => {
        if (!response.ok) {
            throw new Error('Si è verificato un errore nel tentativo di effettuare la richiesta.');
        }
        return response.json();
    })

    .then(data => {
        console.log('Risposta dal server:', data);
        if (data.success) {
            alert('Messaggio inviato!');
        } else {
            alert('Messaggio non inviato: ' + data.message);
        }
    })

    .catch(error => {
        console.error('Si è verificato un errore durante il tentativo di elaborare i dati:', error);
        alert(error);
    });
}