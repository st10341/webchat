// In questo file verranno acquisiti gli utenti con cui l'utente loggato ha interagito

// Chiamata alla funzione 
getAmici();

function getAmici() 
{
    // Dati da inviare al server
    var dati = {
        azione: "getAmici"
    };

    // Opzioni per la richiesta
    var opzioni = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(dati)
    };

    // Esegui una richiesta al servizio Web per ottenere l'elenco degli utenti
    fetch('http://localhost/webchat/WS/UTENTI/read.php')

    .then(response => {
        if (!response.ok) {
            throw new Error('Network response was not ok ' + response.statusText);
        }
        return response.json(); // Supponendo che il server risponda con JSON
    })

    .then(data => {
        // Dati ottenuti dalla richiesta
        // Ora invia i dati alla pagina contatto.php tramite POST
        const formData = new FormData();
        formData.append('users', JSON.stringify(data)); // Converti i dati in formato JSON e aggiungili al FormData

        // Invia la richiesta POST
        fetch('http://localhost/webchat/TEMPL/CONTATTO/contatto.php', {
            method: 'POST',
            body: formData
        })

        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok ' + response.statusText);
            }
        })

        .catch(error => {
            console.error('There was a problem with the POST request:', error);
        });
    })
    .catch(error => {
        console.error('There was a problem with the fetch operation:', error);
    });

}