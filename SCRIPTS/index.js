//in questo file assegno un evento click ad ogni bottone della index e richiamo la funzione adeguata


//EVENTO CLICK BOTTONE LOGIN
document.addEventListener("DOMContentLoaded", function() {
    var bottone = document.getElementById("LOGIN");
          
    bottone.addEventListener("click", effettuaLogin);
});

//EVENTO CLICK BOTTONE REGISTER
document.addEventListener("DOMContentLoaded", function() {
    var bottone = document.getElementById("SIGN-UP");
            
    bottone.addEventListener("click", effettuaRegister);
});




function effettuaRegister()
{
    alert("reg");
    console.log("reg");

    //acquisisco i valori compilati dal form
    var dati = 
    {
        azione: "register",
        nome: document.getElementById("nome").value,
        cognome: document.getElementById("cognome").value,
        dataNascita: document.getElementById("dataNascita").value,
        userName: document.getElementById("userName").value,
        email: document.getElementById("email").value,
        password: document.getElementById("password").value
    };


    // Stabilisco la modalità di comunicazione con il server
    var opzioni = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(dati)
    };

    // Richiesta al server
    fetch('http://localhost/webchat/WS/utenti/create.php', opzioni)

    .then(response => {
        if (!response.ok) {
            throw new Error('Si è verificato un errore nel tentativo di effettuare la richiesta.');
        }
        return response.json();
    })

    .then(data => {
        console.log('Risposta dal server:', data);
        if (data.success) {
            alert('Login riuscito!');
        } else {
            alert('Login fallito: ' + data.message);
        }
    })

    .catch(error => {
        console.error('Si è verificato un errore durante il tentativo di elaborare i dati:', error);
        alert(error);
    });
}


function effettuaLogin()
{
    var dati = {
        azione: "login",
        email: document.getElementById("LogEmail").value,
        password: document.getElementById("LogPassword").value
    };

    if (!dati.email || !dati.password) {
        alert('Per favore, compila entrambi i campi email e password.');
        return;
    }

    var opzioni = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(dati)
    };

    fetch('http://localhost/webchat/WS/utenti/read.php', opzioni)

    .then(response => {
        if (!response.ok) {
            throw new Error('Si è verificato un errore nel tentativo di effettuare la richiesta.');
        }
        return response.json();
    })

    .then(data => {
        console.log('Risposta dal server:', data);
        if (data.success) {
            alert('Login riuscito!');
        } else {
            alert('Login fallito: ' + data.message);
        }
    })

    .catch(error => {
        console.error('Si è verificato un errore durante il tentativo di elaborare i dati:', error);
        alert(error);
    });
}
