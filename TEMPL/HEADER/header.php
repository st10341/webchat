<?php

    if ($_SERVER["REQUEST_METHOD"] == "POST")   //click logout
    { 
        // Cancellazione del cookie
        $path = '/'; // Deve essere uguale a quello usato per impostare il cookie
        $domain = $_SERVER['HTTP_HOST']; // Deve essere uguale a quello usato per impostare il cookie
        $secure = isset($_SERVER['HTTPS']); // Deve essere uguale a quello usato per impostare il cookie
        $httponly = true; // Deve essere uguale a quello usato per impostare il cookie

        // Imposta il cookie con una data di scadenza nel passato per cancellarlo
        setcookie('utenteLoggato', '', time() - 3600, $path, $domain, $secure, $httponly);

        //viene cancellato l'utente dalla sessione
        session_start();
        unset($_SESSION["nickUtente"]);
        session_write_close();   

        //reindirizzamento alla login
        header("Location: /webchat/index.php");
    }
    else
    {
        // Disabilita la visualizzazione dei notice
        error_reporting(E_ALL & ~E_NOTICE);
        // Acquire numero di richieste di amicizia arrivate
        session_start();
        $pathRequire = $_SESSION['pathRequire'];
        $utente = $_SESSION['nickUtente'];
        session_write_close();

        // Require fx db
        require_once $pathRequire . '/PHP/mysql.php';

        $count = Database::countRowsTWO("richieste_amicizia", "nickDestinatario = '$utente'", "orarioAccettata = '2000-01-01 00:00:00'", 'AND');
    }
?>

<link rel="stylesheet" href="/webchat/TEMPL/HEADER/header.css">

<header class="rounded-4 p-3">
    <div class="titolo">

        <div class="text">
            <h4>CHAT WAVE</h4>
        </div>

        <div class="contenitore">
            <img src="/webchat/IMG/chat.png" onclick="document.location='/webchat/PHP/CHAT/chat.php'">
        </div>
        

        <div class="contenitore">
            <img class="immagine" src="/webchat/IMG/user.png" onclick="document.location='/webchat/PHP/RICHIESTA_AMICIZIA/VISUALIZZA_RICHIESTA/visualizzaRichiesta.php'">
            <div class="cerchio">
                <p id="numRichieste"><?php echo $count; ?></p>
            </div>
        </div>
        
        <div class="contenitore">
            <img src="/webchat/IMG/add-user.png" onclick="document.location='/webchat/PHP/RICHIESTA_AMICIZIA/INVIA_RICHIESTA/inviaRichiesta.php'">
        </div>

        <div class="contenitore">
            <form action="/webchat/TEMPL/HEADER/header.php" method="post" style="margin: 0;">
                <input id="logout" type="submit" value="">
            </form>
        </div>
      
    </div>
</header>


