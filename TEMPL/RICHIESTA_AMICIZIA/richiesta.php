

<?php

    session_start();
    // Includi la classe Database
    require_once $_SESSION['pathRequire'] . '/PHP/mysql.php';

    require_once $_SESSION['pathRequire'] . '/PHP/DMO/richieste_amicizia.inc.php';

    $destinatario = $_SESSION['nickUtente'];


    // if($_SERVER['REQUEST_METHOD'] == 'POST') // Richieste dalla richiesta ws
    // {
    //     // Leggi i dati inviati tramite la richiesta POST
    //     $postData = file_get_contents('php://input');

    //     // Decodifica i dati JSON inviati
    //     $richieste = json_decode($postData, true);
    // }
    // else
    // {
    //     // URL del web service
    //     $url = 'http://localhost/webchat/PHP/BL/UTENTE/utentePostBack.php';

    //     // Dati da inviare
    //     $data = array(
    //         'richiesta' => 'acquisisciRichiesteAmic',
    //         'utente' => $_SESSION['nickUtente']
    //     );

    //     // Codifica i dati come stringa JSON
    //     $data_json = json_encode($data);

    //     // Inizializza una sessione cURL
    //     $ch = curl_init($url);

    //     // Imposta le opzioni della richiesta cURL
    //     curl_setopt($ch, CURLOPT_POST, true);
    //     curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //     curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    //         'Content-Type: application/json',
    //         'Content-Length: ' . strlen($data_json)
    //     ));

    //     // Ottieni i cookies della sessione
    //     $sessionCookies = session_id();

    //     // Imposta l'opzione CURLOPT_COOKIE per includere i cookies nella richiesta cURL
    //     curl_setopt($ch, CURLOPT_COOKIE, session_name() . '=' . $sessionCookies);

    //     // Esegui la richiesta e ottieni la risposta
    //     $response = curl_exec($ch);

    //     // Controlla se c'è un errore nella richiesta
    //     if ($response === FALSE) {
    //         // Gestione degli errori cURL
    //         die('Errore nella richiesta cURL: ' . curl_error($ch));
    //     }

    //     // Chiudi la sessione cURL
    //     curl_close($ch);

    //     // Gestisci la risposta
    //     echo $response;
    // }

    //creo il vettore per inserire le richieste
    $richieste = array();
    $destinatario = $_SESSION['nickUtente'];

    //dato il nickName di un utente, mi acquisisco tutte le richieste che gli sono arrivate oppure quelle inviate

    //acquisisco le richieste che sono arrivate a quell'utente
    $res = Database::eseguiSelect("richieste_amicizia", "nickMittente, orarioInvio", "nickDestinatario = '$destinatario'", "orarioAccettata = '2000-01-01 00:00:00'", "AND");
    
    //ciclo sui risultati
    foreach ($res as $risultato)
    {
        //istanzio la classe delle variabili
        $variabili = new clsRichiesteAmiciziaDMO();

        //mi salvo i valori
        $variabili->setOrarioInvio($risultato['orarioInvio']);
        $variabili->setNickMittente($risultato['nickMittente']);

        //inserisco i valori nel vettore
        $richieste[] = $variabili;
        
    }

?>

<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="/webchat/TEMPL/RICHIESTA_AMICIZIA/richiesta.css">
</head>
<body>

    
    <?php foreach ($richieste as $richiesta):?>

        <div class="contatto">
            <div class="imgProfilo">
                <img src="/webchat/IMG/user.png" alt="">
            </div>

            <div class="informazioniContatto">

            
                
            <p style="" name="mittente"><?php echo $richiesta->getNickMittente(); ?></p>

                <form id="amiciziaForm" action="/webchat/PHP/BL/UTENTE/utentePostBack.php" method="POST">
                    
                    <div class="buttons">

                        <input type="hidden" name="mittente" value="<?php echo $richiesta->getNickMittente(); ?>">
                        <input type="hidden" name="destinatario" value="<?php echo $destinatario; ?>">
                        
                        <div class="divs" style="margin-right: 5px;">
                            <input type="submit" name="accetta" id="accetta" value="">
                        </div>


                        <div class="divs">
                            <input type="submit" name="rifiuta" id="rifiuta" value="">
                        </div>
                        
                    </div>
                </form>
        
            </div>

        </div>
    
    <?php endforeach; ?>

    <?php if(count($richieste) == 0): ?>
        <p id="errore">Non ti sono arrivate richieste di amicizia!</p>
    <?php endif; ?>


    
</body>
</html>

