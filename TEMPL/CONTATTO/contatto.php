<link rel="stylesheet" href="/webchat/TEMPL/CONTATTO/contatto.css">

<?php
    session_start();

    require_once $_SESSION['pathRequire'] . '/PHP/BL/UTENTE/utente_metodi.php';
    require_once $_SESSION['pathRequire'] . '/PHP/global.php';

    // Invio di una richiesta POST per acquisire tutti gli utenti con cui ha interagito il loggato
    $utenti = clsUtenteBL::acquisiciAmici();


    if($_SERVER['REQUEST_METHOD'] == 'POST')   
    {
        if(isset($_POST['amico']))    // È stato cliccato un utente dalla lista
        {
            session_start();
            $_SESSION['amicoCliccato'] = $_POST['amico'];
            header("Location: /webchat/PHP/CHAT/chat.php");
        }
    }

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <?php if(isset($utenti)): ?>
        <?php foreach ($utenti as $utente): ?>

            <form style="margin:0;" method="POST" action="/webchat/TEMPL/CONTATTO/contatto.php">

                <button style="margin-bottom: 10px;" type="submit" id="btnContatto" name="amico">

                    <div class="imgProfilo">
                        <img src="/webchat/IMG/user.png">
                    </div>

                    <div class="informazioniContatto">
                        <input type="hidden" name="amico" value="<?php echo $utente; ?>">
                        <p><?php echo $utente ?></p>
                        <hr>
                        <p>Ultimo messaggio</p>
                    </div>

                </button>

                <div class="contatto">

                </div>

            </form>

        <?php endforeach; ?>
    <?php endif; ?>


</body>
</html>