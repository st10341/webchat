<link rel="stylesheet" href="/webchat/TEMPL/AMICO/amico.css">

<?php

    if ($_SERVER["REQUEST_METHOD"] == "POST") 
    {
        session_start();

        //inserisco in sessione il nickName dell'amico con cui voglio avviare una chat
        $_SESSION['amicoCliccato'] = $_POST['amico'];
        unset($_SESSION['amico']);

        //reindirizzo alla chat
        header("Location: /webchat/PHP/CHAT/chat.php");
    }
    else //acquisisce tutti gli amici dal DB
    {
        // Includi la classe Database
        require_once '../../PHP/mysql.php';


        session_start();

        function acquisisciRichieste($destinatario)
        {
            // Istanzia l'oggetto Database
            $database = new Database();

            //creo il vettore per inserire le richieste
            $richieste = array();


            //acquisisco le richieste di amicizia che l'utente ha accettato, quindi i suoi amici
            $res = $database->eseguiSelect("richieste_amicizia", "*", "orarioAccettata <> '2000-01-01 00:00:00'", "nickMittente = '$destinatario' OR nickDestinatario = '$destinatario'", "AND");

            //ciclo sui risultati
            foreach ($res as $risultato)
            {
                //mi salvo il nickName del mittente e del destinatario della richiesta di amicizia
                $nickMittente = $risultato['nickMittente'];
                $nickDestinatario = $risultato['nickDestinatario'];

                //controllo se il mittente è uguale al nome presente in sessione, se vero restituisco il nickName del destinatario
                if ($nickMittente == $_SESSION['nickUtente']) $richieste[] = $nickDestinatario;
                else $richieste[] = $nickMittente;
            }

            //restituisco il vettore
            return $richieste;
        }

        // Chiama la funzione per ottenere gli amici dell'utente loggato
        $richieste = acquisisciRichieste($_SESSION['nickUtente']);
    }

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link rel="stylesheet" href="/webchat/TEMPL/AMICO/amico.css">

</head>
<body>

    <?php if(count($richieste) > 0): ?>
        <?php foreach ($richieste as $richiesta): ?>
            
            <div class="contatto" id="contatto-<?php echo $index; ?>">

                <form name="salvaAmico" action="/webchat/TEMPL/AMICO/amico.php" method="post">

                    <div class="imgProfilo">
                        <img src="/webchat/IMG/user.png" alt="">
                    </div>

                    
                    <div class="informazioniContatto">
                        <input type="hidden" name="amico" value="<?php echo $richiesta; ?>">
                        <p><?php echo $richiesta; ?></p>
                        <hr>
                        <input type="submit" class="btn" value="Avvia Chat">
                    </div>

                </form>

            </div>


        <?php endforeach; ?>
    <?php else: ?>
    
        <p style="color: white; text-align:center; margin-bottom:0;">Non hai ancora nessun amico! </p>

    <?php endif; ?>

</body>
</html>