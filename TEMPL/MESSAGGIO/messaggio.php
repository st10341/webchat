<link rel="stylesheet" href="/webchat/TEMPL/MESSAGGIO/messaggio.css">

<?php
    session_start();
    $pathRequire = $_SESSION['pathRequire'];
    $pathWS = $_SESSION['pathWS'];

    if (isset($_SESSION['amicoCliccato']))
    {
        $amicoCliccato = $_SESSION['amicoCliccato'];
        //unset($_SESSION['amicoCliccato']);
    }

    session_write_close();

    // Include la classe utenteBL
    require_once  $pathRequire. '/PHP/BL/UTENTE/utente_metodi.php';



    if($_SERVER['REQUEST_METHOD'] == 'POST') // Messaggi arrivati dalla richiesta ws
    {
        // Leggi i dati inviati tramite la richiesta POST
        $postData = file_get_contents('php://input');

        // Decodifica i dati JSON inviati
        $messaggi = json_decode($postData, true);
    }
    else
    {
        // URL del web service
        $url = 'http://' . $pathWS . '/webchat/PHP/BL/UTENTE/utentePostBack.php';

        // Dati da inviare
        $data = array(
            'richiesta' => 'getMsg',
            'utente1' => $_SESSION['amicoCliccato'],
            'utente2' => $_SESSION['nickUtente']
        );

        // Codifica i dati come stringa JSON
        $data_json = json_encode($data);

        // Inizializza una sessione cURL
        $ch = curl_init($url);

        // Imposta le opzioni della richiesta cURL
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_json)
        ));

        // Ottieni i cookies della sessione
        $sessionCookies = session_id();

        // Imposta l'opzione CURLOPT_COOKIE per includere i cookies nella richiesta cURL
        curl_setopt($ch, CURLOPT_COOKIE, "PHPSESSID=" . $sessionCookies);

        // Esegui la richiesta e ottieni la risposta
        $response = curl_exec($ch);

        // Controlla se c'è un errore nella richiesta
        if ($response === FALSE) {
            die('Errore nella richiesta cURL: ' . curl_error($ch));
        }

        // Chiudi la sessione cURL
        curl_close($ch);

        // Gestisci la risposta
        echo $response;
    }
?>

<?php if(isset($messaggi)) : ?>

    <?php foreach ($messaggi as $messaggio): ?>

        <?php if($messaggio[1] == $_SESSION["nickUtente"]): ?>

            <div id="riga" class="divDx">
                <div id="blocco">

                    <!-- <p><?php echo $messaggio[1]; ?></p>
                    <hr> -->
                    <p><?php echo $messaggio[0]; ?></p>

                </div>
            </div>

        <?php else: ?>

            <div id="riga" class="divSx">
                <div id="blocco">

                    <!-- <p><?php echo $messaggio[1]; ?></p>
                    <hr> -->
                    <p><?php echo $messaggio[0]; ?></p>

                </div>
            </div>
        
        <?php endif; ?>


    <?php endforeach; ?>

    <?php endif; ?>
